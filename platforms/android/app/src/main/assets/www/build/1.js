webpackJsonp([1],{

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyInputPageModule", function() { return SurveyInputPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__survey_input__ = __webpack_require__(694);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SurveyInputPageModule = /** @class */ (function () {
    function SurveyInputPageModule() {
    }
    SurveyInputPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__survey_input__["a" /* SurveyInputPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__survey_input__["a" /* SurveyInputPage */]),
            ],
        })
    ], SurveyInputPageModule);
    return SurveyInputPageModule;
}());

//# sourceMappingURL=survey-input.module.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyInputPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_toast_service__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__survey_map_survey_map__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_photo_viewer__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_maps_service__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SurveyInputPage = /** @class */ (function () {
    function SurveyInputPage(camera, navCtrl, navParams, toast, actionSheetCtrl, geolocation, photoViewer, mapsServiceProvider) {
        this.camera = camera;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.actionSheetCtrl = actionSheetCtrl;
        this.geolocation = geolocation;
        this.photoViewer = photoViewer;
        this.mapsServiceProvider = mapsServiceProvider;
        this.formSurvey = {
            isSuccess: true,
            value: [
                {
                    name: 'Hasil Survey Rumah',
                    tag: 'HasilSurvey',
                    orderIdx: 1,
                    details: [
                        {
                            id: 1,
                            type: 'select',
                            form: 'AlamatSesuaiId',
                            label: 'Alamat Sesuai',
                            error: 'Alamat sesuai harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'AlamatSekarang',
                                label: 'Alamat Sekarang',
                                error: 'Alamat sekarang tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 2,
                            type: 'select',
                            form: 'AlamatDitemukanId',
                            label: 'Alamat Ditemukan',
                            error: 'Alamat Ditemukan harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 3,
                            type: 'select',
                            form: 'AlamatDitemukanKantorKosongId',
                            label: 'Alamat Ditemukan Tetapi Kantor Kosong/Pindah',
                            error: 'Alamat Ditemukan Tetapi Kantor Kosong harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 4,
                            type: 'select',
                            form: 'AlamatDitemukanAplikanTidakDikenalId',
                            label: 'Alamat Ditemukan Tetapi Aplikan Tidak Dikenal',
                            error: 'Alamat Ditemukan Tetapi Aplikan Tidak Dikenal harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                }
                            ],
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Hasil Wawancara Pihak Pertama (SI 1)',
                    tag: 'WawancaraPihak1_Rumah',
                    orderIdx: 2,
                    details: [
                        {
                            id: 5,
                            type: 'text',
                            form: 'Nama',
                            label: 'Nama',
                            error: 'Nama tidak boleh kosong!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 6,
                            type: 'select',
                            form: 'JenisKelaminId',
                            label: 'Jenis Kelamin',
                            error: 'Jenis Kelamin harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Laki-Laki'
                                },
                                {
                                    id: 2,
                                    label: 'Perempuan'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 7,
                            type: 'select',
                            form: 'HubunganSumberInformasiId',
                            label: 'Hubungan Sumber Informasi',
                            error: 'Hubungan SumberInformasi harus dipilih',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'HubunganSumberInformasiLainnya',
                                label: null,
                                error: 'Hubungan Sumber Informasi Lainnya tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Aplikan'
                                },
                                {
                                    id: 2,
                                    label: 'Suami/Istri'
                                },
                                {
                                    id: 3,
                                    label: 'Anak'
                                },
                                {
                                    id: 4,
                                    label: 'Saudara'
                                },
                                {
                                    id: 5,
                                    label: 'Kakak/Adik'
                                },
                                {
                                    id: 6,
                                    label: 'Orang Tua'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 8,
                            type: 'select',
                            form: 'LingkunganRumahId',
                            label: 'Lingkungan Rumah',
                            error: 'Lingkungan Rumah harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Real/Estate'
                                },
                                {
                                    id: 2,
                                    label: 'Ruko/Rukan'
                                },
                                {
                                    id: 3,
                                    label: 'Apartemen'
                                },
                                {
                                    id: 4,
                                    label: 'Perumahan'
                                },
                                {
                                    id: 5,
                                    label: 'Lingkungan Biasa'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 9,
                            type: 'select',
                            form: 'TipeTempatTinggalId',
                            label: 'Tipe Tempat Tinggal',
                            error: 'Tipe Tempat Tinggal harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'TipeTempatTinggalLainnya',
                                label: null,
                                error: 'Tipe Tempat Tinggal Lainnya tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Rumah'
                                },
                                {
                                    id: 2,
                                    label: 'Apartemen'
                                },
                                {
                                    id: 3,
                                    label: 'Pabrik'
                                },
                                {
                                    id: 4,
                                    label: 'Gudang'
                                },
                                {
                                    id: 5,
                                    label: 'Ruko'
                                },
                                {
                                    id: 6,
                                    label: 'Rumah Susun'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 10,
                            type: 'select',
                            form: 'LokasiRumahId',
                            label: 'Lokasi Rumah',
                            error: 'Lokasi Rumah harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Jalan Raya'
                                },
                                {
                                    id: 2,
                                    label: 'Ruko/Rukan'
                                },
                                {
                                    id: 3,
                                    label: 'Kawasan Industri'
                                },
                                {
                                    id: 4,
                                    label: 'Gang, Masuk Mobil'
                                },
                                {
                                    id: 5,
                                    label: 'Gang, Tidak Masuk Mobil'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 11,
                            type: 'select',
                            form: 'KondisiJalanId',
                            label: 'Kondisi Jalan',
                            error: 'Kondisi Jalan harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Aspal/Beton/Permanen'
                                },
                                {
                                    id: 2,
                                    label: 'Tanah/Tidak Permanen'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 12,
                            type: 'select',
                            form: 'JenisBangunanId',
                            label: 'Jenis Bangunan',
                            error: 'Jenis Bangunan harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Permanen'
                                },
                                {
                                    id: 2,
                                    label: 'Semi Permanen'
                                },
                                {
                                    id: 3,
                                    label: 'Non Permanen/Bedeng Triplek'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 13,
                            type: 'inputWithLabel',
                            form: null,
                            label: 'Jumlah Tanggungan',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'JumlahTanggungan',
                                label: 'Orang',
                                error: 'Jumlah Tanggungan tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 14,
                            type: 'select',
                            form: 'StatusPerkawinanId',
                            label: 'Status Perkawinan',
                            error: 'Status Perkawinan harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Kawin'
                                },
                                {
                                    id: 2,
                                    label: 'Belum Kawin'
                                },
                                {
                                    id: 3,
                                    label: 'Janda/Duda'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 15,
                            type: 'inputWithLabel',
                            form: null,
                            label: 'Lama Tinggal',
                            error: 'Lama Tinggal harus diisi',
                            placeholder: null,
                            labelDivider: null,
                            ext1: {
                                type: 'number',
                                form: 'LamaTinggalTahun',
                                label: 'Tahun',
                                error: 'Lama Tinggal Tahun tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: {
                                type: 'number',
                                form: 'LamaTinggalBulan',
                                label: 'Bulan',
                                error: 'Lama Tinggal Bulan tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 16,
                            type: 'select',
                            form: 'AplikanTinggalDisiniId',
                            label: 'Aplikan Tinggal Disini',
                            error: 'Aplikan Tinggal Disini harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                },
                                {
                                    id: 3,
                                    label: 'Tidak Tahu'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 17,
                            type: 'number',
                            form: 'UsiaAplikan',
                            label: 'Usia Aplikan',
                            error: 'Usia Aplikan tidak boleh kosong!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 18,
                            type: 'select',
                            form: 'PendidikanAplikanId',
                            label: 'Pendidikan Aplikan',
                            error: 'Pendidikan Aplikan harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'SD'
                                },
                                {
                                    id: 2,
                                    label: 'SMP'
                                },
                                {
                                    id: 3,
                                    label: 'SMU'
                                },
                                {
                                    id: 4,
                                    label: 'D3'
                                },
                                {
                                    id: 5,
                                    label: 'S1'
                                },
                                {
                                    id: 6,
                                    label: 'S2'
                                },
                                {
                                    id: 7,
                                    label: 'S3'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 19,
                            type: 'tel',
                            form: 'NoHpAplikan',
                            label: 'No Hp Aplikan',
                            error: 'No Hp Aplikan tidak boleh kosong!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 20,
                            type: 'text',
                            form: 'NamaKantorAplikan',
                            label: 'Nama Kantor Aplikan',
                            error: 'Nama Kantor Aplikan tidak boleh kosong!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 21,
                            type: 'multiselect',
                            form: 'FasilitasRumahId',
                            label: 'Fasilitas Rumah',
                            error: 'Fasilitas Rumah harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'FasilitasRumahLainnya',
                                label: null,
                                error: 'Fasilitas Rumah Lainnya tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: {
                                type: 'tel',
                                form: 'FasilitasRumahNomorTelp',
                                label: null,
                                error: 'No Telp tidak boleh kosong!',
                                placeHolder: 'No Telp',
                                value: null
                            },
                            ext3: null,
                            data: [
                                {
                                    id: 102,
                                    label: 'Telepon'
                                },
                                {
                                    id: 1,
                                    label: 'Taman'
                                },
                                {
                                    id: 2,
                                    label: 'Carport'
                                },
                                {
                                    id: 3,
                                    label: 'Garasi'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 22,
                            type: 'select',
                            form: 'StatusKepemilikanId',
                            label: 'Status Kepemilikan',
                            error: 'Status Kepemilikan harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'StatusKepemilikanLainnya',
                                label: null,
                                error: 'Status Kepemilikan Lainnya tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: {
                                type: 'text',
                                form: 'StatusKepemilikanBerakhirnyaKontrak',
                                label: null,
                                error: 'Status KepemilikanBerakhirnya Kontrak tidak boleh kosong!',
                                placeHolder: 'Berakhirnya Kontrak',
                                value: null
                            },
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Milik Sendiri'
                                },
                                {
                                    id: 2,
                                    label: 'Milik Keluarga'
                                },
                                {
                                    id: 3,
                                    label: 'Rumah Dinas'
                                },
                                {
                                    id: 102,
                                    label: 'Sewa/Kost'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 23,
                            type: 'inputWithLabel',
                            form: null,
                            label: 'Jumlah Penghuni',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'JumlahPenghuni',
                                label: 'Orang',
                                error: 'Jumlah penghuni tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 24,
                            type: 'text',
                            form: 'NamaIbuKandung',
                            label: 'Nama Ibu Kandung',
                            error: 'Nama Ibu Kandung tidak boleh kosong!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 25,
                            type: 'text',
                            form: 'NamaSuamiIstri',
                            label: 'Nama Suami/Istri',
                            error: 'Nama Suami/Istri tidak boleh kosong!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 26,
                            type: 'select',
                            form: 'PekerjaanPasanganId',
                            label: 'Pekerjaan Pasangan',
                            error: 'Pekerjaan Pasangan harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'PekerjaanPasanganLainnya',
                                label: null,
                                error: 'Pekerjaan Pasangan Lainnya tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Karyawan'
                                },
                                {
                                    id: 2,
                                    label: 'Profesional'
                                },
                                {
                                    id: 3,
                                    label: 'Pensiun'
                                },
                                {
                                    id: 4,
                                    label: 'TNI/Polri'
                                },
                                {
                                    id: 5,
                                    label: 'Wiraswasta'
                                },
                                {
                                    id: 6,
                                    label: 'Pegawai Negeri'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 27,
                            type: 'text',
                            form: 'NamaKantorPasangan',
                            label: 'Nama Kantor Pasangan Aplikan',
                            error: 'Nama Kantor Pasangan Aplikan tidak boleh kosong!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 28,
                            type: 'select',
                            form: 'KapasitasListrikId',
                            label: 'Kapasitas Listrik',
                            error: 'Kapasitas Listrik harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: '450 Watt'
                                },
                                {
                                    id: 2,
                                    label: '900 Watt'
                                },
                                {
                                    id: 3,
                                    label: '1300 Watt'
                                },
                                {
                                    id: 4,
                                    label: '2200 Watt'
                                },
                                {
                                    id: 5,
                                    label: '2200 Watt'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 29,
                            type: 'select',
                            form: 'KepemilikanKartuKreditId',
                            label: 'Kepemilikan Kartu Kredit',
                            error: 'Kepemilikan Kartu Kredit harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'JenisKartuKredit',
                                label: null,
                                error: 'Jenis Kartu Kredit tidak boleh kosong!',
                                placeHolder: 'Jenis Kartu Kredit',
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 101,
                                    label: 'Ya'
                                },
                                {
                                    id: 1,
                                    label: 'Tidak'
                                }
                            ],
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Hasil Wawancara Pihak Kedua (SI 2)',
                    tag: 'WawancaraPihak2_Rumah',
                    orderIdx: 3,
                    details: [
                        {
                            id: 30,
                            type: 'text',
                            form: 'Nama',
                            label: 'Nama',
                            error: 'Nama tidak boleh kosong!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 31,
                            type: 'select',
                            form: 'JenisKelaminId',
                            label: 'Jenis Kelamin',
                            error: 'Jenis Kelamin harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Laki-Laki'
                                },
                                {
                                    id: 2,
                                    label: 'Perempuan'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 32,
                            type: 'select',
                            form: 'HubunganSumberInformasiId',
                            label: 'Hubungan Sumber Informasi',
                            error: 'Hubungan SumberInformasi harus dipilih',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'HubunganSumberInformasiLainnya',
                                label: 'Hubungan Sumber Informasi Lainnya',
                                error: 'Hubungan Sumber Informasi Lainnya tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: {
                                type: 'text',
                                form: 'NamaKetuaRT',
                                label: 'No Rumah Tetangga',
                                error: 'No Rumah Tetanggatidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext3: {
                                type: 'number',
                                form: 'NoRumahTetangga',
                                label: null,
                                error: null,
                                placeHolder: null,
                                value: null
                            },
                            data: [
                                {
                                    id: 102,
                                    label: 'Ketua RT'
                                },
                                {
                                    id: 103,
                                    label: 'Tetangga'
                                },
                                {
                                    id: 1,
                                    label: 'Security Perumahan'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 33,
                            type: 'select',
                            form: 'AplikanDikenalId',
                            label: 'Aplikan Dikenal',
                            error: 'Aplikan Dikenal harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KeteranganAplikanDikenal',
                                label: 'Keterangan Aplikan',
                                error: 'Keterangan Aplikan tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 34,
                            type: 'select',
                            form: 'AplikanTinggalDiAlamatTsbId',
                            label: 'Aplikan Tinggal Di Alamat Tersebut',
                            error: 'Aplikan Tinggal Di Alamat Tersebut harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KeteranganAplikanTinggal',
                                label: 'Keterangan Alamat Aplikan',
                                error: 'Keterangan Alamat Aplikan tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak Tahu'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 35,
                            type: 'select',
                            form: 'StatusKepemilikanId',
                            label: 'Status Kepemilikan',
                            error: 'Status Kepemilikan harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KeteranganStatusKepemilikan',
                                label: 'Keterangan Status Kepemilikan',
                                error: 'Keterangan Status Kepemilikan tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Sesuai'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak Tahu'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 36,
                            type: 'select',
                            form: 'LamaTinggalId',
                            label: 'Lama Tinggal',
                            error: 'Lama Tinggal harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KeteranganLamaTinggal',
                                label: 'Keterangan Lama Tinggal',
                                error: 'Keterangan Lama Tinggal tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Sesuai'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak Tahu'
                                }
                            ],
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Hasil Wawancara Pihak Ketiga (SI 3)',
                    tag: 'WawancaraPihak3_Rumah',
                    orderIdx: 4,
                    details: [
                        {
                            id: 37,
                            type: 'text',
                            form: 'Nama',
                            label: 'Nama',
                            error: 'Nama harus diisi!',
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 38,
                            type: 'select',
                            form: 'JenisKelaminId',
                            label: 'Jenis Kelamin',
                            error: 'Jenis Kelamin harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Laki-Laki'
                                },
                                {
                                    id: 2,
                                    label: 'Perempuan '
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 39,
                            type: 'select',
                            form: 'HubunganSumberInformasiId',
                            label: 'Hubungan Sumber Informasi',
                            error: 'Hubungan SumberInformasi harus dipilih',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'HubunganSumberInformasiLainnya',
                                label: 'Hubungan Sumber Informasi Lainnya',
                                error: 'Hubungan sumber informasi lainnya harus diisi!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: {
                                type: 'text',
                                form: 'NamaKetuaRT',
                                label: 'Nama Ketua RT',
                                error: 'Nama ketua RT harus diisi!',
                                placeHolder: null,
                                value: null
                            },
                            ext3: {
                                type: 'number',
                                form: 'NoRumahTetangga',
                                label: 'No Rumah Tetangga',
                                error: 'No rumah tetangga harus diisi!',
                                placeHolder: null,
                                value: null
                            },
                            data: [
                                {
                                    id: 102,
                                    label: 'Ketua RT'
                                },
                                {
                                    id: 103,
                                    label: 'Tetangga'
                                },
                                {
                                    id: 1,
                                    label: 'Security Perumahan'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 40,
                            type: 'select',
                            form: 'AplikanDikenalId',
                            label: 'Aplikan Dikenal',
                            error: 'Aplikan Dikenal harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'AplikanDikenalKeterangan',
                                label: 'Keterangan Aplikan',
                                error: 'Ketarangan aplikan harus diisi!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 41,
                            type: 'select',
                            form: 'AplikanTinggalDiAlamatTsbId',
                            label: 'Aplikan Tinggal Di Alamat Tersebut',
                            error: 'Aplikan Tinggal Di Alamat Tersebut harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KeteranganAplikanTinggal',
                                label: 'Keterangan Alamat Aplikan',
                                error: 'Keterangan alamat aplikan harus diisi!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Ya'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak Tahu'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 42,
                            type: 'select',
                            form: 'StatusKepemilikanId',
                            label: 'Status Kepemilikan',
                            error: 'Status Kepemilikan harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KeteranganStatusKepemilikan',
                                label: 'Keterangan Status Kepemilikan',
                                error: 'Keterangan status kepemilikan harus diisi!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Sesuai'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak Tahu'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 43,
                            type: 'select',
                            form: 'LamaTinggalId',
                            label: 'Lama Tinggal',
                            error: 'Lama Tinggal harus di pilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KeteranganLamaTinggal',
                                label: 'keterangan Lama Tinggal',
                                error: 'Keterangan lama tinggal harus diisi!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Sesuai'
                                },
                                {
                                    id: 2,
                                    label: 'Tidak'
                                },
                                {
                                    id: 101,
                                    label: 'Tidak Tahu'
                                }
                            ],
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Hasil Pengamatan Saat Survey',
                    tag: 'PengamatanSaatSurvey_Rumah',
                    orderIdx: 5,
                    details: [
                        {
                            id: 44,
                            type: 'select',
                            form: 'KondisiBangunanRumahId',
                            label: 'Kondisi Bangunan Rumah',
                            error: 'Kondisi bangunan rumah harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Baik/Terawat'
                                },
                                {
                                    id: 2,
                                    label: 'Sedang/Kurang Terawat'
                                },
                                {
                                    id: 3,
                                    label: 'Rusak/Tidak Terawat'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 45,
                            type: 'inputWithLabel',
                            form: null,
                            label: 'Luas Tanah',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: {
                                type: 'number',
                                form: 'LuasTanahPanjang',
                                label: 'M',
                                error: 'Panjang tanah tidak boleh kosong!',
                                placeHolder: 'Panjang',
                                value: null
                            },
                            ext2: {
                                type: 'number',
                                form: 'LuasTanahLebar',
                                label: 'M',
                                error: 'Lebar tanah tidak boleh kosong!',
                                placeHolder: 'Lebar',
                                value: null
                            },
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 46,
                            type: 'inputWithLabel',
                            form: null,
                            label: 'Luas Bangunan',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: {
                                type: 'number',
                                form: 'LuasBangunanPanjang',
                                label: 'M',
                                error: 'Panjang bangunan tidak boleh kosong!',
                                placeHolder: 'Panjang',
                                value: null
                            },
                            ext2: {
                                type: 'number',
                                form: 'LuasBangunanLebar',
                                label: 'M',
                                error: 'Lebar bangunan tidak boleh kosong!',
                                placeHolder: 'Lebar',
                                value: null
                            },
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 47,
                            type: 'select',
                            form: 'JumlahTingkatId',
                            label: 'Jumlah Tingkat',
                            error: 'Jumlah tingkat harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: '1'
                                },
                                {
                                    id: 2,
                                    label: '2'
                                },
                                {
                                    id: 3,
                                    label: '3'
                                },
                                {
                                    id: 4,
                                    label: '>3'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 48,
                            type: 'inputWithLabel',
                            form: null,
                            label: 'Lebar Jalan',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: {
                                type: 'number',
                                form: 'LebarJalan',
                                label: 'M',
                                error: 'Lebar jalan tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 49,
                            type: 'inputWithLabel',
                            form: null,
                            label: 'Jumlah Lantai Rumah',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: {
                                type: 'number',
                                form: 'JumlahLantaiRumah',
                                label: 'Tingkat',
                                error: 'Jumlah lantai rumah tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 50,
                            type: 'select',
                            form: 'KondisiDindingRumahId',
                            label: 'Kondisi Dinding Rumah',
                            error: 'Kondisi dinding rumah harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KondisiDindingRumahLainnya',
                                label: 'Kondisi Dinding Rumah Lainnya',
                                error: 'Kondisi dinding rumah lainnya tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Tembok Diplester'
                                },
                                {
                                    id: 2,
                                    label: 'Tembok Tidak Diplester'
                                },
                                {
                                    id: 3,
                                    label: 'Sebagian/Seluruhnya Tidak Disemen'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 51,
                            type: 'select',
                            form: 'KondisiAtapRumahId',
                            label: 'Kondisi Atap Rumah',
                            error: 'Kondisi atap rumah harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'text',
                                form: 'KondisiAtapRumahLainnya',
                                label: 'Kondisi Atap Rumah Lainnya',
                                error: 'Kondisi atap rumah lainnya tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Genteng'
                                },
                                {
                                    id: 2,
                                    label: 'Asbes/Seng'
                                },
                                {
                                    id: 3,
                                    label: 'Kayu/Sirap'
                                },
                                {
                                    id: 101,
                                    label: 'Lainnya'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 52,
                            type: 'select',
                            form: 'KendaraanYangDimilikiId',
                            label: 'Kendaraan Yang Dimiliki',
                            error: 'Kendaraan Yang Dimiliki harus dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: {
                                type: 'number',
                                form: 'JumlahKendaraan',
                                label: 'Jumlah Kendaraan',
                                error: 'Jumlah kendaraan tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext2: {
                                type: 'text',
                                form: 'JenisKendaraan',
                                label: 'Jenis Kendaraan',
                                error: 'Jenis kendaraan tidak boleh kosong!',
                                placeHolder: null,
                                value: null
                            },
                            ext3: null,
                            data: [
                                {
                                    id: 1012,
                                    label: 'Ya'
                                },
                                {
                                    id: 1,
                                    label: 'Tidak'
                                }
                            ],
                            value: ''
                        },
                        {
                            id: 53,
                            type: 'select',
                            form: 'PencarianAlamatId',
                            label: 'Pencarian Alamat',
                            error: 'Pencarian alamat perlu dipilih!',
                            placeholder: 'Pilih...',
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: [
                                {
                                    id: 1,
                                    label: 'Mudah'
                                },
                                {
                                    id: 2,
                                    label: 'Sulit'
                                },
                                {
                                    id: 3,
                                    label: 'Tidak Terlacak'
                                }
                            ],
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Map Rumah',
                    tag: 'SurveyMap',
                    orderIdx: 6,
                    details: [
                        {
                            id: 54,
                            type: 'map',
                            form: null,
                            label: 'Map',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: {
                                type: 'decimal',
                                form: 'Longitude',
                                label: 'Longitude',
                                error: 'Longitude harus diisi! Klik pada map untuk mengisi.',
                                placeHolder: null,
                                value: null
                            },
                            ext2: {
                                type: 'decimal',
                                form: 'Latitude',
                                label: 'Latitude',
                                error: 'Latitude harus diisi! Klik pada map untuk mengisi.',
                                placeHolder: null,
                                value: null
                            },
                            ext3: null,
                            data: null,
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Upload Gambar Rumah',
                    tag: 'SurveyDocument',
                    orderIdx: 7,
                    details: [
                        {
                            id: 55,
                            type: 'upload',
                            form: 'UploadUrl',
                            label: 'Upload',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Surveyor',
                    tag: 'SurveyorInfo',
                    orderIdx: 8,
                    details: [
                        {
                            id: 56,
                            type: 'readOnlyText',
                            form: 'Code',
                            label: 'Kode',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 57,
                            type: 'readOnlyText',
                            form: 'Nama',
                            label: 'Nama',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 58,
                            type: 'readOnlyText',
                            form: 'Tanggal',
                            label: 'Tanggal',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        },
                        {
                            id: 59,
                            type: 'readOnlyText',
                            form: 'Jam',
                            label: 'Jam',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Catatan',
                    tag: 'SurveyNote',
                    orderIdx: 9,
                    details: [
                        {
                            id: 60,
                            type: 'textarea',
                            form: 'Catatan',
                            label: 'Catatan',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        }
                    ]
                },
                {
                    name: 'Informasi Tambahan',
                    tag: 'InformasiTambahan',
                    orderIdx: 10,
                    details: [
                        {
                            id: 61,
                            type: 'textarea',
                            form: 'InformasiTambahan',
                            label: 'Informasi Tambahan',
                            error: null,
                            placeholder: null,
                            labelDivider: null,
                            ext1: null,
                            ext2: null,
                            ext3: null,
                            data: null,
                            value: ''
                        }
                    ]
                }
            ]
        };
        this.geolocationMaps = {};
        this.surveyImage = [];
        this.surveyTampil = {};
        this.specialExt = [101, 102, 103, 1021];
        this.extData = { ext1: 101, ext2: 102, ext3: 103, ext12: 1012 };
        this.lengthFormSurvey = 0;
        this.lenghtEachStep = {};
        this.surveyMultiSelect = [];
        this.surveyMultiSelectId = [];
        this.index = {
            form: 0,
            field: 0
        };
        this.isExt1 = false;
        this.isExt2 = false;
        this.isExt3 = false;
    }
    SurveyInputPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.lengthFormSurvey = this.formSurvey.value.length;
        var index = 0;
        this.formSurvey.value.map(function (x) {
            _this.lenghtEachStep[index] = x.details.length;
            index++;
        });
        this.tampilkanSurvey();
        // this.geolocationMaps = this.mapsServiceProvider.geolocationMyLocation();
        this.geolocationMyLocation();
    };
    SurveyInputPage.prototype.checkExtMultiselect = function (data) {
        if (this.specialExt.includes(data.id)) {
            return true;
        }
        else {
            return false;
        }
    };
    SurveyInputPage.prototype.geolocationSave = function () {
        this.formSurvey.value[this.index.form].details[this.index.field].ext1.value = this.geolocationMaps.lat;
        this.formSurvey.value[this.index.form].details[this.index.field].ext2.value = this.geolocationMaps.lng;
    };
    SurveyInputPage.prototype.geolocationMyLocation = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__["d" /* LocationService */].getMyLocation().then(function (myLocation) {
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                "position": myLocation.latLng
            }).then(function (results) {
                if (results.length == 0) {
                    return null;
                }
                var address = [
                    results[0].subThoroughfare || "",
                    results[0].thoroughfare || "",
                    results[0].locality || "",
                    results[0].adminArea || "",
                    results[0].postalCode || "",
                    results[0].country || ""
                ].join(", ");
                _this.geolocationMaps = myLocation.latLng;
                _this.geolocationMaps["address"] = address;
            });
        });
    };
    SurveyInputPage.prototype.showImage = function (i) {
        this.photoViewer.show(this.surveyImage[i]);
    };
    // closeMaps = function(data) {
    //   return new Promise((resolve, reject) => {
    //           resolve(this.geolocationMaps= data);
    //       });
    // }
    SurveyInputPage.prototype.openMaps = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__survey_map_survey_map__["a" /* SurveyMapPage */]);
    };
    SurveyInputPage.prototype.surveyRemoveImage = function (i) {
        this.surveyImage.splice(i, 1);
    };
    //modified
    SurveyInputPage.prototype.tampilkanSurvey = function () {
        this.surveyTampil = this.formSurvey.value[this.index.form].details[this.index.field];
        this.surveyTampil["name"] = this.formSurvey.value[this.index.form].name;
    };
    SurveyInputPage.prototype.checkExtSelect = function (data) {
        this.isExt1 = data.id === this.extData.ext1 || data.id === this.extData.ext12;
        this.isExt2 = data.id === this.extData.ext2 || data.id === this.extData.ext12;
        this.isExt3 = data.id === this.extData.ext3;
    };
    //modified
    SurveyInputPage.prototype.select = function (data) {
        this.formSurvey.value[this.index.form].details[this.index.field].value = data.label;
        this.checkExtSelect(data);
    };
    SurveyInputPage.prototype.multiselect = function (data) {
        if (this.surveyMultiSelect.includes(data.label)) {
            var index = this.surveyMultiSelect.indexOf(data.label);
            this.surveyMultiSelect.splice(index, 1);
            this.surveyMultiSelectId.splice(index, 1);
        }
        else {
            this.surveyMultiSelect.push(data.label);
            this.surveyMultiSelectId.push(data.id);
        }
        console.log(this.surveyMultiSelect);
        this.formSurvey.value[this.index.form].details[this.index.field].value = this.surveyMultiSelect;
        this.isExt1 = this.surveyMultiSelectId.includes(101) || this.surveyMultiSelectId.includes(1012);
        this.isExt2 = this.surveyMultiSelectId.includes(102) || this.surveyMultiSelectId.includes(1012);
        this.isExt3 = data.id === 103;
        console.log("ext1" + this.isExt1 + "ext2" + this.isExt2 + "ext3" + this.isExt3);
    };
    SurveyInputPage.prototype.next = function () {
        // //modified
        console.log(this.formSurvey.value[this.index.form].details[this.index.field]);
        var surveyInput = this.formSurvey.value[this.index.form].details[this.index.field];
        var allowInputWithLabel = false;
        console.log(surveyInput);
        if (surveyInput.type == "map") {
            this.geolocationSave();
        }
        if (this.surveyImage.length < 4 && surveyInput.type == "upload") {
            this.toast.showTopToast(surveyInput.error);
            return;
        }
        else if (this.surveyImage.length >= 4 && surveyInput.type == "upload") {
            surveyInput.value = this.surveyImage;
        }
        if (!surveyInput.value) {
            if (this.isExt1 == true || this.isExt2 == true || this.isExt3 == true) {
            }
            else {
                if (surveyInput.ext1 !== null || surveyInput.ext2 !== null || surveyInput.ext3 !== null) {
                    if (surveyInput.ext1 !== null && !surveyInput.ext1.value) {
                        this.toast.showTopToast(surveyInput.ext1.error);
                        return;
                    }
                    else {
                        allowInputWithLabel = true;
                    }
                    if (surveyInput.ext2 !== null && !surveyInput.ext2.value) {
                        this.toast.showTopToast(surveyInput.ext2.error);
                        return;
                    }
                    else {
                        allowInputWithLabel = true;
                    }
                    if (surveyInput.ext3 !== null && !surveyInput.ext3.value) {
                        this.toast.showTopToast(surveyInput.ext3.error);
                        return;
                    }
                    else {
                        allowInputWithLabel = true;
                    }
                }
                else {
                    if (this.formSurvey.value[this.index.form].tag == "SurveyorInfo" || this.formSurvey.value[this.index.form].tag == "SurveyNote" || this.formSurvey.value[this.index.form].tag == "InformasiTambahan") {
                    }
                    else {
                        this.toast.showTopToast(surveyInput.error);
                        return;
                    }
                }
            }
        }
        if (this.isExt1 == true && !surveyInput.ext1.value) {
            this.toast.showTopToast(surveyInput.ext1.error);
            return;
        }
        else if (allowInputWithLabel == false && this.isExt1 == false && surveyInput.ext1 !== null) {
            surveyInput.ext1.value = null;
        }
        if (this.isExt2 == true && !surveyInput.ext2.value) {
            this.toast.showTopToast(surveyInput.ext2.error);
            return;
        }
        else if (allowInputWithLabel == false && this.isExt2 == false && surveyInput.ext2 !== null) {
            surveyInput.ext2.value = null;
        }
        if (this.isExt3 == true && !surveyInput.ext3.value) {
            this.toast.showTopToast(surveyInput.ext3.error);
            return;
        }
        else if (allowInputWithLabel == false && this.isExt3 == false && surveyInput.ext3 !== null) {
            surveyInput.ext3.value = null;
        }
        if (this.index.field + 1 >= this.lenghtEachStep[this.index.form] && this.index.form + 1 >= this.lengthFormSurvey)
            alert("Survey Complete! Terima Kasih");
        if (this.index.field + 1 >= this.lenghtEachStep[this.index.form]) {
            this.index.form++;
            this.index.field = 0;
        }
        else {
            this.index.field++;
        }
        this.isExt1 = false;
        this.isExt2 = false;
        this.isExt3 = false;
        this.tampilkanSurvey();
    };
    SurveyInputPage.prototype.prev = function () {
        if (this.index.field <= 0 && this.index.form <= 0)
            return;
        if (this.index.field <= 0) {
            this.index.form--;
            //modified
            this.index.field = this.formSurvey.value[this.index.form].details.length - 1;
        }
        else {
            this.index.field--;
        }
        var surveyInput = this.formSurvey.value[this.index.form].details[this.index.field];
        this.isExt1 = surveyInput.ext1 !== null && surveyInput.ext1.value;
        this.isExt2 = surveyInput.ext2 !== null && surveyInput.ext2.value;
        this.isExt3 = surveyInput.ext3 !== null && surveyInput.ext3.value;
        this.tampilkanSurvey();
    };
    SurveyInputPage.prototype.ambilGambar = function () {
        var _this = this;
        var actionsheet = this.actionSheetCtrl.create({
            title: "Pilih Gambar",
            buttons: [{
                    text: "Dari Device",
                    handler: function () {
                        var options = {
                            quality: 70,
                            destinationType: _this.camera.DestinationType.DATA_URL,
                            sourceType: _this.camera.PictureSourceType.PHOTOLIBRARY,
                            saveToPhotoAlbum: false
                        };
                        _this.camera.getPicture(options).then(function (imageData) {
                            var dataCamera = 'data:image/jpeg;base64,' + imageData;
                            _this.surveyImage.push(dataCamera);
                        }, function (err) {
                            // Handle error
                        });
                    }
                },
                {
                    text: "Dari Kamera",
                    handler: function () {
                        var options = {
                            quality: 70,
                            destinationType: _this.camera.DestinationType.DATA_URL,
                            encodingType: _this.camera.EncodingType.JPEG,
                            mediaType: _this.camera.MediaType.PICTURE
                        };
                        _this.camera.getPicture(options).then(function (imageData) {
                            var dataCamera = 'data:image/jpeg;base64,' + imageData;
                            _this.surveyImage.push(dataCamera);
                        }, function (err) {
                            // Handle error
                        });
                    }
                }, {
                    text: "Cancel",
                    role: "cancel"
                }]
        });
        actionsheet.present();
    };
    SurveyInputPage.prototype.uploadGambar = function () {
        alert("upload gambar");
        //to api gambar
    };
    SurveyInputPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-survey-input',template:/*ion-inline-start:"C:\Users\mr k\testpull\ponnymobileedit\src\pages\survey-input\survey-input.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Nama Aplikan</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card>\n    <h3 text-center padding>{{surveyTampil.name}}</h3>\n  </ion-card>\n  <ion-grid *ngIf="surveyTampil.type == \'select\'">\n    <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n    <div class="surveyOuterDiv" *ngFor="let itemData of surveyTampil.data" (click)="select(itemData)">\n      <div [ngClass]="{\'surveyLeftDiv\': surveyTampil, \'surveyLeftDivSelected\': (surveyTampil.value == itemData.label) && (isExt1 || isExt2 || isExt3)}">\n        <div\n          [ngClass]="{\'circle-selected\': surveyTampil.value == itemData.label,\'circle\': surveyTampil.value !== itemData.label}">\n        </div>\n      </div>\n      <div class="surveyRightDiv">\n        <ion-grid>\n          <ion-row>\n            <div>\n              <span [ngClass]="{\'selectedSurvey\': surveyTampil.value == itemData.label}">{{itemData.label}}</span>\n            </div>\n          </ion-row>\n          <ion-row>\n\n              <div style="width: 100%" *ngIf="isExt1">\n                  <div *ngIf="surveyTampil.value == itemData.label">\n                    <ion-item no-padding>\n                      <ion-textarea *ngIf="surveyTampil.ext1.type == \'textarea\'" placeholder="{{surveyTampil.ext1.placeholder}}" [(ngModel)]="surveyTampil.ext1.value">\n                      </ion-textarea>\n                      <ion-input *ngIf="surveyTampil.ext1.type !== \'textarea\'" placeholder="{{surveyTampil.ext1.placeholder}}" [(ngModel)]="surveyTampil.ext1.value"></ion-input>\n                    </ion-item>\n                  </div>\n                </div>\n\n                <div style="width: 100%" *ngIf="isExt2">\n                  <div *ngIf="surveyTampil.value == itemData.label">\n                    <ion-item no-padding >\n                         <ion-textarea *ngIf="surveyTampil.ext2.type == \'textarea\'" placeholder="{{surveyTampil.ext2.placeholder}}" [(ngModel)]="surveyTampil.ext2.value">\n                      </ion-textarea>\n                      <ion-input *ngIf="surveyTampil.ext2.type !== \'textarea\'" placeholder="{{surveyTampil.ext2.placeholder}}" [(ngModel)]="surveyTampil.ext2.value"></ion-input>\n                    </ion-item>\n                  </div>\n                </div>\n\n                <div style="width: 100%" *ngIf="isExt3">\n                  <div no-padding *ngIf="surveyTampil.value == itemData.label">\n                    <ion-item>\n                      <ion-textarea *ngIf="surveyTampil.ext3.type == \'textarea\'" placeholder="{{surveyTampil.ext3.placeholder}}" [(ngModel)]="surveyTampil.ext3.value">\n                      </ion-textarea>\n                      <ion-input *ngIf="surveyTampil.ext3.type !== \'textarea\'" placeholder="{{surveyTampil.ext3.placeholder}}" [(ngModel)]="surveyTampil.ext3.value"></ion-input>\n                    </ion-item>\n                  </div>\n                </div>\n          </ion-row>\n        </ion-grid>\n\n\n\n    </div>\n\n    </div>\n\n    </ion-grid>\n\n  <ion-grid *ngIf="surveyTampil.type == \'multiselect\'">\n      <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n      <div class="surveyOuterDiv" *ngFor="let itemData of surveyTampil.data">\n\n        <div (click)="multiselect(itemData)" [ngClass]="{\'surveyLeftDiv\': surveyTampil, \'surveyLeftDivSelected\':  (surveyTampil.value.includes(itemData.label)) && (isExt1 || isExt2 || isExt3) && checkExtMultiselect(itemData)}">\n          <div\n            [ngClass]="{\'circle-selected\': (surveyTampil.value.includes(itemData.label)),\'circle\': (surveyTampil.value.includes(itemData.label)) !== itemData.label}">\n          </div>\n        </div>\n\n        <div class="surveyRightDiv">\n          <ion-grid>\n            <ion-row>\n              <div>\n                <span [ngClass]="{\'selectedSurvey\': (surveyTampil.value.includes(itemData.label))}">{{itemData.label}}</span>\n              </div>\n            </ion-row>\n            <ion-row>\n\n                <div style="width: 100%" *ngIf="isExt1">\n                    <div *ngIf="(surveyTampil.value.includes(itemData.label)) && checkExtMultiselect(itemData) && itemData.id == 101">\n                      <ion-item no-padding>\n                        <ion-textarea *ngIf="surveyTampil.ext1.type == \'textarea\'" placeholder="{{surveyTampil.ext1.placeholder}}" [(ngModel)]="surveyTampil.ext1.value">\n                        </ion-textarea>\n                        <ion-input *ngIf="surveyTampil.ext1.type !== \'textarea\'" placeholder="{{surveyTampil.ext1.placeholder}}" [(ngModel)]="surveyTampil.ext1.value"></ion-input>\n                      </ion-item>\n                    </div>\n                  </div>\n\n                  <div style="width: 100%" *ngIf="isExt2">\n                    <div *ngIf="(surveyTampil.value.includes(itemData.label)) && checkExtMultiselect(itemData) && itemData.id == 102 ">\n                      <ion-item no-padding >\n                           <ion-textarea *ngIf="surveyTampil.ext2.type == \'textarea\'" placeholder="{{surveyTampil.ext2.placeholder}}" [(ngModel)]="surveyTampil.ext2.value">\n                        </ion-textarea>\n                        <ion-input *ngIf="surveyTampil.ext2.type !== \'textarea\'" placeholder="{{surveyTampil.ext2.placeholder}}" [(ngModel)]="surveyTampil.ext2.value"></ion-input>\n                      </ion-item>\n                    </div>\n                  </div>\n\n                  <div style="width: 100%" *ngIf="isExt3">\n                    <div no-padding *ngIf="(surveyTampil.value.includes(itemData.label)) && checkExtMultiselect(itemData) && itemData.id == 103">\n                      <ion-item>\n                        <ion-textarea *ngIf="surveyTampil.ext3.type == \'textarea\'" placeholder="{{surveyTampil.ext3.placeholder}}" [(ngModel)]="surveyTampil.ext3.value">\n                        </ion-textarea>\n                        <ion-input *ngIf="surveyTampil.ext3.type !== \'textarea\'" placeholder="{{surveyTampil.ext3.placeholder}}" [(ngModel)]="surveyTampil.ext3.value"></ion-input>\n                      </ion-item>\n                    </div>\n                  </div>\n            </ion-row>\n          </ion-grid>\n\n\n\n      </div>\n\n      </div>\n\n      </ion-grid>\n\n  <div *ngIf="surveyTampil.type == \'textarea\'">\n    <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n    <div class="surveyOuterDiv">\n    <ion-item>\n      <ion-textarea [(ngModel)]="surveyTampil.value" placeholder="{{surveyTampil.placeholder}}" rows="5"></ion-textarea>\n    </ion-item>\n  </div>\n  </div>\n\n  <div *ngIf="surveyTampil.type == \'text\' || surveyTampil.type == \'number\' || surveyTampil.type==\'tel\'">\n    <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n    <div class="surveyOuterDiv">\n      <ion-item>\n        <ion-input [type]="surveyTampil.type" placeholder="{{surveyTampil.placeholder}}" [(ngModel)]="surveyTampil.value"></ion-input>\n      </ion-item>\n    </div>\n  </div>\n\n\n  <div *ngIf="surveyTampil.type == \'readOnlyText\'">\n    <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n    <div class="surveyOuterDiv">\n      <ion-item>\n        <ion-input type="text" disabled [(ngModel)]="surveyTampil.value"></ion-input>\n      </ion-item>\n    </div>\n  </div>\n\n\n  <!-- <div *ngIf="surveyTampil.type == \'textarea\'">\n    <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n    <ion-card>\n      <ion-item>\n        <ion-textarea [(ngModel)]="surveyTampil.value"></ion-textarea>\n      </ion-item>\n    </ion-card>\n  </div> -->\n\n  <!-- <div *ngIf="surveyTampil.type == \'text\' || surveyTampil.type == \'number\' || surveyTampil.type==\'tel\'">\n    <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n    <ion-card>\n      <ion-item>\n        <ion-input [type]="surveyTampil.type" [(ngModel)]="surveyTampil.value"></ion-input>\n      </ion-item>\n    </ion-card>\n  </div> -->\n\n\n  <div *ngIf="surveyTampil.type == \'upload\'">\n    <img padding src="../../assets/imgs/addImage.png" width="100" height="100" alt="" class="upload"\n      (click)="ambilGambar()">\n    <div class="surveyOuterDiv">\n        <ion-grid>\n          <ion-row>\n            <ion-col col-4 *ngFor="let image of surveyImage;let i = index">\n              <ion-icon class="icon-close-image" name="close-circle" (click)="surveyRemoveImage(i)" end></ion-icon>\n              <img class="image-round" height="100" width="100" *ngIf="image" src="{{image}}" (click)="showImage(i)">\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n    </div>\n  </div>\n\n  <div *ngIf="surveyTampil.type == \'map\'">\n      <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n    <div class="surveyOuterDiv">\n      <ion-grid>\n      <h4>Latitude  : {{geolocationMaps.lat}}</h4>\n      <h4>Longitude : {{geolocationMaps.lng}}</h4>\n      <p>Alamat     : {{geolocationMaps.address}}</p>\n      <img src="https://maps.googleapis.com/maps/api/staticmap?center={{geolocationMaps.lat}}%2c%20{{geolocationMaps.lng}}&zoom=12&size=400x400&key=AIzaSyD9SRw6P1z5T0CoiPIIuM17lh5urhKksXQ"  (click)="openMaps()">\n    </ion-grid>\n      </div>\n  </div>\n\n  <div *ngIf="surveyTampil.type == \'inputWithLabel\'">\n      <div padding text-wrap class="surveyLabel">{{surveyTampil.label}}</div>\n    <div class="surveyOuterDiv">\n      <ion-item>\n        <ion-label *ngIf="surveyTampil.ext1.placeholder" color="primary" stacked>{{surveyTampil.ext1.placeholder}}\n        </ion-label>\n        <div item-end>\n          <ion-label>{{surveyTampil.ext1.label}}</ion-label>\n        </div>\n        <ion-input type="text" [(ngModel)]="surveyTampil.ext1.value"></ion-input>\n      </ion-item>\n\n      <ion-item *ngIf="surveyTampil.ext2 !== null">\n        <ion-label *ngIf="surveyTampil.ext2.placeholder" color="primary" stacked>{{surveyTampil.ext2.placeholder}}\n        </ion-label>\n        <div item-end>\n          <ion-label>{{surveyTampil.ext2.label}}</ion-label>\n        </div>\n        <ion-input type="text" [(ngModel)]="surveyTampil.ext2.value"></ion-input>\n      </ion-item>\n\n    </div>\n  </div>\n\n  <ion-row>\n    <ion-col>\n      <button ion-button color="kembali" class="surveyBtnKembali" (click)="prev()"\n        [disabled]="index.form <1 && index.field <1">Kembali</button>\n    </ion-col>\n    <ion-col>\n      <button ion-button color="selanjutnya" class="surveyBtnSelanjutnya" (click)="next(surveyTampil)"\n        style="float:right">Selanjutnya</button>\n    </ion-col>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mr k\testpull\ponnymobileedit\src\pages\survey-input\survey-input.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_toast_service__["a" /* ToastServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_photo_viewer__["a" /* PhotoViewer */], __WEBPACK_IMPORTED_MODULE_7__providers_maps_service__["a" /* MapsServiceProvider */]])
    ], SurveyInputPage);
    return SurveyInputPage;
}());

//# sourceMappingURL=survey-input.js.map

/***/ })

});
//# sourceMappingURL=1.js.map