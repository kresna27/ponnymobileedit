webpackJsonp([4],{

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_ErrorObservable__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_ErrorObservable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_ErrorObservable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_config__ = __webpack_require__(665);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__principal_service__ = __webpack_require__(130);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, principal) {
        this.http = http;
        this.principal = principal;
        this.networkIsConnected = true;
        this.url = __WEBPACK_IMPORTED_MODULE_4__config_config__["a" /* Config */].API_URL;
        /**
         * Network Checker
         *
         
        if (this.network.type === 'none' || this.network.type === 'unknown') this.networkIsConnected = false;
          
        this.network.onDisconnect().subscribe(() => {
          this.networkIsConnected = false;
        });
          
        this.network.onConnect().subscribe(() => {
          this.networkIsConnected = true;
        });
          
        if (!window['cordova']) this.networkIsConnected = true;
        
        */
    }
    ApiServiceProvider.prototype.getUrl = function () {
        return window['IonicDevServer'] ? 'api' : this.url;
    };
    ApiServiceProvider.prototype.get = function (endpoint, httpOptions, isJson) {
        if (isJson === void 0) { isJson = true; }
        // if (!this.networkIsConnected) {
        //   return new ErrorObservable('Network error, make sure there are internet connection.');
        // }
        var options = this.createRequestHeader(httpOptions ? httpOptions : {}, isJson);
        return this.http.get(this.getUrl() + endpoint, options).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.handleError));
    };
    ApiServiceProvider.prototype.post = function (endpoint, body, httpOptions, isJson) {
        if (isJson === void 0) { isJson = true; }
        // if (!this.networkIsConnected) {
        //   return new ErrorObservable('Network error, make sure there are internet connection.');
        // }
        var options = this.createRequestHeader(httpOptions ? httpOptions : {}, isJson);
        return this.http.post(this.getUrl() + endpoint, body, options).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.handleError));
    };
    ApiServiceProvider.prototype.delete = function (endpoint, httpOptions) {
        // if (!this.networkIsConnected) {
        //   return new ErrorObservable('Network error, make sure there are internet connection.');
        // }
        var options = this.createRequestHeader(httpOptions ? httpOptions : {});
        return this.http.delete(this.getUrl() + endpoint, options).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.handleError));
    };
    ApiServiceProvider.prototype.createRequestHeader = function (options, isJson) {
        if (isJson === void 0) { isJson = true; }
        var headers = options.hasOwnProperty('headers') ? options.headers : {};
        if (isJson) {
            headers['Content-Type'] = 'application/json';
        }
        if (this.principal.getToken()) {
            headers['Authorization'] = "Bearer " + this.principal.getToken();
        }
        headers['Access-Control-Allow-Origin'] = '*';
        options.headers = headers;
        return options;
    };
    ApiServiceProvider.prototype.handleError = function (error) {
        if (error.hasOwnProperty('error')) {
            if (error.error instanceof ErrorEvent) {
                return new __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_ErrorObservable__["ErrorObservable"](error.error.message);
            }
            else
                return new __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_ErrorObservable__["ErrorObservable"](error.status.toString() + ': ' + error.statusText);
        }
        else
            return new __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_ErrorObservable__["ErrorObservable"]("Terjadi kesalahan pada sistem.");
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_5__principal_service__["a" /* PrincipalServiceProvider */]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrincipalServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_login__ = __webpack_require__(154);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PrincipalServiceProvider = /** @class */ (function () {
    function PrincipalServiceProvider() {
        this.isLogin = false;
        this.userInfo = new __WEBPACK_IMPORTED_MODULE_1__models_login__["b" /* LoginResponse */]();
    }
    PrincipalServiceProvider.prototype.setUser = function (user) {
        var date = new Date(user.expiration).getTime().toString();
        localStorage.setItem('token', user.token);
        localStorage.setItem('expired', date);
        this.userInfo = user;
        this.isLogin = true;
    };
    PrincipalServiceProvider.prototype.getToken = function () {
        return this.userInfo.token;
    };
    PrincipalServiceProvider.prototype.getLoginStatus = function () {
        return this.isLogin;
    };
    PrincipalServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], PrincipalServiceProvider);
    return PrincipalServiceProvider;
}());

//# sourceMappingURL=principal.service.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToastServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToastServiceProvider = /** @class */ (function () {
    function ToastServiceProvider(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ToastServiceProvider.prototype.showTopToast = function (content, duration, closeButton, closeButtonText) {
        if (duration === void 0) { duration = 5000; }
        if (closeButton === void 0) { closeButton = true; }
        if (closeButtonText === void 0) { closeButtonText = 'x'; }
        var toast = this.toastCtrl.create({
            message: content,
            position: 'top',
            duration: duration,
            showCloseButton: closeButton,
            closeButtonText: closeButtonText
        });
        toast.present();
    };
    ToastServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
    ], ToastServiceProvider);
    return ToastServiceProvider;
}());

//# sourceMappingURL=toast.service.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LoginResponse; });
var LoginRequest = /** @class */ (function () {
    function LoginRequest() {
    }
    return LoginRequest;
}());

var LoginResponse = /** @class */ (function () {
    function LoginResponse() {
    }
    return LoginResponse;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_service__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__loading_service__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_login__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__toast_service__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__principal_service__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config_endpoint__ = __webpack_require__(311);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var AuthServiceProvider = /** @class */ (function () {
    function AuthServiceProvider(apiService, loading, toast, principal) {
        this.apiService = apiService;
        this.loading = loading;
        this.toast = toast;
        this.principal = principal;
        this.authNotifier = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["BehaviorSubject"](null);
        this.isLogin = false;
    }
    AuthServiceProvider.prototype.tokenChecker = function () {
        var token = localStorage.getItem('token');
        /**
         * TODO: Check token validity
         */
        if (token) {
            this.reauthenticate(token);
        }
        else {
            this.authNotifier.next(false);
        }
    };
    AuthServiceProvider.prototype.reauthenticate = function (token) {
        return __awaiter(this, void 0, void 0, function () {
            var tempUserData, response, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        tempUserData = new __WEBPACK_IMPORTED_MODULE_4__models_login__["b" /* LoginResponse */]();
                        tempUserData.token = token;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.apiService.post(__WEBPACK_IMPORTED_MODULE_7__config_endpoint__["a" /* EndPoint */].ACCOUNTS.AUTHENTICATE, {}).toPromise()];
                    case 2:
                        response = _a.sent();
                        if (!response.isSuccess) {
                            this.toast.showTopToast(response.errorMessage);
                        }
                        else {
                            this.principal.setUser(response.value);
                            this.authNotifier.next(true);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        this.toast.showTopToast(error_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AuthServiceProvider.prototype.authenticate = function (credentials) {
        return __awaiter(this, void 0, void 0, function () {
            var response, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loading.startLoading();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.apiService.post(__WEBPACK_IMPORTED_MODULE_7__config_endpoint__["a" /* EndPoint */].ACCOUNTS.LOGIN, credentials).toPromise()];
                    case 2:
                        response = _a.sent();
                        if (!response.isSuccess) {
                            this.toast.showTopToast(response.errorMessage);
                        }
                        else {
                            this.loading.stopLoading();
                            this.principal.setUser(response.value);
                            this.authNotifier.next(true);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        this.toast.showTopToast(error_2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AuthServiceProvider.prototype.logout = function () { };
    AuthServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__loading_service__["a" /* LoadingServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5__toast_service__["a" /* ToastServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_6__principal_service__["a" /* PrincipalServiceProvider */]])
    ], AuthServiceProvider);
    return AuthServiceProvider;
}());

//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ 167:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 167;

/***/ }),

/***/ 211:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/login/login.module": [
		688,
		2
	],
	"../pages/survey-input/survey-input.module": [
		691,
		1
	],
	"../pages/survey-list/survey-list.module": [
		689,
		0
	],
	"../pages/survey-map/survey-map.module": [
		690,
		3
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 211;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadingServiceProvider = /** @class */ (function () {
    function LoadingServiceProvider(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    LoadingServiceProvider.prototype.startLoading = function (label, spinner) {
        if (label === void 0) { label = ''; }
        if (spinner === void 0) { spinner = 'crescent'; }
        this.loading = this.loadingCtrl.create({
            content: label,
            spinner: spinner
        });
        this.loading.present();
    };
    LoadingServiceProvider.prototype.stopLoading = function () {
        this.loading.dismissAll();
    };
    LoadingServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], LoadingServiceProvider);
    return LoadingServiceProvider;
}());

//# sourceMappingURL=loading.service.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EndPoint; });
var Account = /** @class */ (function () {
    function Account() {
    }
    return Account;
}());
var Survey = /** @class */ (function () {
    function Survey() {
    }
    return Survey;
}());
var EndPoint = /** @class */ (function () {
    function EndPoint() {
    }
    EndPoint.ACCOUNTS = {
        LOGIN: '/accounts/login',
        AUTHENTICATE: '/accounts/authenticate'
    };
    EndPoint.SURVEYS = {
        LISTSURVEY: '/inputorder'
    };
    return EndPoint;
}());

//# sourceMappingURL=endpoint.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyMapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SurveyMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SurveyMapPage = /** @class */ (function () {
    function SurveyMapPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.geolocationMaps = {};
    }
    SurveyMapPage.prototype.ionViewDidLoad = function () {
        this.callback = this.navParams.get("callback");
        this.loadMap();
    };
    SurveyMapPage.prototype.done = function () {
        var _this = this;
        this.callback(this.geolocationMaps).then(function () {
            _this.navCtrl.pop();
        });
    };
    SurveyMapPage.prototype.loadMap = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["d" /* LocationService */].getMyLocation().then(function (myLocation) {
            var options = {
                camera: {
                    target: myLocation.latLng,
                    zoom: 18,
                    tilt: 30
                }
            };
            _this.map = __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMaps */].create(_this.mapElement.nativeElement, options);
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                "position": myLocation.latLng
            }).then(function (results) {
                if (results.length == 0) {
                    return null;
                }
                var address = [
                    results[0].subThoroughfare || "",
                    results[0].thoroughfare || "",
                    results[0].locality || "",
                    results[0].adminArea || "",
                    results[0].postalCode || "",
                    results[0].country || ""
                ].join(", ");
                _this.marker = _this.map.addMarkerSync({
                    title: address,
                    icon: 'blue',
                    animation: 'DROP',
                    position: myLocation.latLng
                });
                _this.marker.on(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["c" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function () {
                });
            });
            // this.map.one(GoogleMapsEvent.MAP_READY)
            // .then(() => {
            //   console.log('Map is ready!');
            //   this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
            //       (data) => {
            //         if(this.marker) this.marker.remove();
            //         Geocoder.geocode({
            //           "position": new LatLng(data[0].lat, data[0].lng)
            //         }).then((results: GeocoderResult[]) => {
            //           if (results.length == 0) {
            //             return null;
            //           }
            //           let address: any = [
            //             results[0].subThoroughfare || "",
            //             results[0].thoroughfare || "",
            //             results[0].locality || "",
            //             results[0].adminArea || "",
            //             results[0].postalCode || "",
            //             results[0].country || ""].join(", ");
            //             this.marker = this.map.addMarkerSync({
            //               title: address,
            //               icon: 'blue',
            //               animation: 'DROP',
            //               position: {
            //                 lat: data[0].lat,
            //                 lng: data[0].lng
            //               }
            //             });
            //             this.marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
            //             });
            //             this.geolocationMaps = {
            //               latitude : data[0].lat,
            //               longitude : data[0].lng,
            //               address : address
            //           }
            //           });
            //       }
            //   );
            // });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("map"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], SurveyMapPage.prototype, "mapElement", void 0);
    SurveyMapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-survey-map',template:/*ion-inline-start:"C:\Users\mr k\testpull\ponnymobileedit\src\pages\survey-map\survey-map.html"*/'<!--\n  Generated template for the SurveyMapPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>surveyMap</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <!-- <button ion-item full (click)="done()">Select</button> -->\n<div #map id="map_canvas"></div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mr k\testpull\ponnymobileedit\src\pages\survey-map\survey-map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], SurveyMapPage);
    return SurveyMapPage;
}());

//# sourceMappingURL=survey-map.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormValidatorServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FormValidatorServiceProvider = /** @class */ (function () {
    function FormValidatorServiceProvider() {
    }
    FormValidatorServiceProvider.prototype.findErrorMessages = function (form) {
        var controls = form.controls;
        var errorMsgList = [];
        for (var key in controls) {
            if (controls[key].valid) {
                continue;
            }
            var errors = controls[key].errors;
            for (var errKey in errors) {
                switch (errKey) {
                    case 'required':
                        errorMsgList.push(key + ' cannot empty.');
                        break;
                    case 'pattern':
                        if (key === 'Password')
                            errorMsgList.push('Pattern ' + key + ' is wrong, only number, word dan symbol "@#$%^&+=" is allowed.');
                        else if (key === 'Email')
                            errorMsgList.push('Pattern ' + key + ' is wrong.');
                        else if (key === 'Handphone')
                            errorMsgList.push('Pattern ' + key + ' is wrong, only 6281xxx or 081xxx is allowed.');
                        break;
                    case 'validateEqual':
                        errorMsgList.push(key + ' is not match.');
                        break;
                    default:
                        break;
                }
            }
        }
        return errorMsgList;
    };
    FormValidatorServiceProvider.prototype.getFirstErrorMessage = function (form) {
        if (!form.valid)
            return this.findErrorMessages(form)[0];
        else
            return;
    };
    FormValidatorServiceProvider.prototype.getAllErrorMessages = function (form) {
        if (!form.valid)
            return this.findErrorMessages(form).join('\n\r');
        else
            return;
    };
    FormValidatorServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], FormValidatorServiceProvider);
    return FormValidatorServiceProvider;
}());

//# sourceMappingURL=form-validator.service.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_service__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_endpoint__ = __webpack_require__(311);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SurveyServiceProvider = /** @class */ (function () {
    function SurveyServiceProvider(apiService) {
        this.apiService = apiService;
        this.surveyData = [];
        this.surveyShowData = {};
        this.extData = { ext1: 101, ext2: 102, ext3: 103, ext12: 1012 };
        this.isExt1 = false;
        this.isExt2 = false;
        this.isExt3 = false;
    }
    SurveyServiceProvider.prototype.getSurveys = function () {
        return this.apiService.get(__WEBPACK_IMPORTED_MODULE_2__config_endpoint__["a" /* EndPoint */].SURVEYS.LISTSURVEY).toPromise();
    };
    SurveyServiceProvider.prototype.nextSurvey = function (data) {
    };
    SurveyServiceProvider.prototype.prevSurvey = function (data) {
    };
    SurveyServiceProvider.prototype.checkSurvey = function (data) {
    };
    SurveyServiceProvider.prototype.checkExtSelect = function (data) {
        switch (data.id) {
            case this.extData.ext1: {
                this.isExt1 = true;
                break;
            }
            case this.extData.ext2: {
                this.isExt2 = true;
                break;
            }
            case this.extData.ext3: {
                this.isExt3 = true;
                break;
            }
            case this.extData.ext12: {
                this.isExt1 = true;
                this.isExt2 = true;
                break;
            }
        }
    };
    SurveyServiceProvider.prototype.selectSurvey = function (data) {
        this.surveyShowData = data.label;
        this.checkExtSelect(data);
    };
    SurveyServiceProvider.prototype.multiselectSurvey = function (data) {
    };
    SurveyServiceProvider.prototype.surveyShow = function (data) {
        this.surveyShowData = data;
    };
    SurveyServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_service__["a" /* ApiServiceProvider */]])
    ], SurveyServiceProvider);
    return SurveyServiceProvider;
}());

//# sourceMappingURL=survey.service.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapsServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_google_maps__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MapsServiceProvider = /** @class */ (function () {
    function MapsServiceProvider(geolocation) {
        this.geolocation = geolocation;
    }
    MapsServiceProvider.prototype.geolocationMyLocation = function () {
        var location = {};
        __WEBPACK_IMPORTED_MODULE_1__ionic_native_google_maps__["d" /* LocationService */].getMyLocation().then(function (myLocation) {
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                "position": myLocation.latLng
            }).then(function (results) {
                if (results.length == 0) {
                    return null;
                }
                var address = [
                    results[0].subThoroughfare || "",
                    results[0].thoroughfare || "",
                    results[0].locality || "",
                    results[0].adminArea || "",
                    results[0].postalCode || "",
                    results[0].country || ""
                ].join(", ");
                location = myLocation.latLng;
                location["address"] = address;
                console.log(location);
                return location;
            });
        });
    };
    MapsServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */]])
    ], MapsServiceProvider);
    return MapsServiceProvider;
}());

//# sourceMappingURL=maps.service.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(365);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(686);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(687);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_service__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_form_validator_service__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_toast_service__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_survey_service__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_principal_service__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_loading_service__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_api_service__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common_http__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_camera__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_google_maps__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_survey_map_survey_map__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_maps_service__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_photo_viewer__ = __webpack_require__(358);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















// import { IonicImageViewerModule } from 'ionic-img-viewer';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */], __WEBPACK_IMPORTED_MODULE_18__pages_survey_map_survey_map__["a" /* SurveyMapPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/survey-list/survey-list.module#SurveyListPageModule', name: 'SurveyListPage', segment: 'survey-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/survey-map/survey-map.module#SurveyMapPageModule', name: 'SurveyMapPage', segment: 'survey-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/survey-input/survey-input.module#SurveyInputPageModule', name: 'SurveyInputPage', segment: 'survey-input', priority: 'low', defaultHistory: [] }
                    ]
                }), __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["b" /* HttpClientModule */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */], __WEBPACK_IMPORTED_MODULE_18__pages_survey_map_survey_map__["a" /* SurveyMapPage */]],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_7__providers_auth_service__["a" /* AuthServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_8__providers_form_validator_service__["a" /* FormValidatorServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_9__providers_toast_service__["a" /* ToastServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_11__providers_principal_service__["a" /* PrincipalServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_12__providers_loading_service__["a" /* LoadingServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_10__providers_survey_service__["a" /* SurveyServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_13__providers_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_19__providers_maps_service__["a" /* MapsServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_photo_viewer__["a" /* PhotoViewer */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 665:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.API_URL = 'http://localhost:50114/api';
    Config.GOOGLE_MAP_API_KEY = 'AIzaSyCNX9Xn6WmecGQAKS9sclNlkqmtwZOZzk8';
    return Config;
}());

//# sourceMappingURL=config.js.map

/***/ }),

/***/ 686:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, authService) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.authService = authService;
        this.platform.ready().then(function () {
            _this.rootPage = 'SurveyInputPage';
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.authCheck();
        });
    }
    MyApp.prototype.authCheck = function () {
        var _this = this;
        if (window['IonicDevServer']) {
            var routeUrl = document.URL.split('/#/');
            if (routeUrl.length > 1)
                window.location.href = routeUrl[0];
        }
        this.authService.authNotifier.filter(function (res) { return res !== null; }).subscribe(function (res) {
            if (res) {
                _this.rootPage = 'SurveyListPage';
            }
            else {
                _this.rootPage = 'LoginPage';
            }
            _this.splashScreen.hide();
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\mr k\testpull\ponnymobileedit\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\mr k\testpull\ponnymobileedit\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthServiceProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 687:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\mr k\testpull\ponnymobileedit\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      Ionic Blank\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  The world is your oyster.\n\n  <p>\n\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will be your guide.\n\n  </p>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mr k\testpull\ponnymobileedit\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[360]);
//# sourceMappingURL=main.js.map