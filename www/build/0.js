webpackJsonp([0],{

/***/ 689:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyListPageModule", function() { return SurveyListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__survey_list__ = __webpack_require__(693);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SurveyListPageModule = /** @class */ (function () {
    function SurveyListPageModule() {
    }
    SurveyListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__survey_list__["a" /* SurveyListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__survey_list__["a" /* SurveyListPage */]),
            ],
        })
    ], SurveyListPageModule);
    return SurveyListPageModule;
}());

//# sourceMappingURL=survey-list.module.js.map

/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_survey_service__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_toast_service__ = __webpack_require__(153);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var SurveyListPage = /** @class */ (function () {
    function SurveyListPage(navCtrl, survey, toast) {
        this.navCtrl = navCtrl;
        this.survey = survey;
        this.toast = toast;
        this.surveys = [];
        this.isLoading = true;
    }
    SurveyListPage.prototype.ionViewDidLoad = function () { };
    SurveyListPage.prototype.getSurveys = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, surveyList, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.survey.getSurveys()];
                    case 1:
                        response = _a.sent();
                        this.isLoading = false;
                        if (!response.isSuccess) {
                            this.toast.showTopToast(response.errorMessage);
                        }
                        else {
                            surveyList = response.value;
                            this.surveys = surveyList.inputOrderList.map(function (item) {
                                var obj = item;
                                var time = new Date(obj.receiptDate);
                                time.setUTCHours(time.getUTCHours() + -time.getTimezoneOffset() / 60);
                                obj.receiptDate = time.toLocaleString();
                                return obj;
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        this.isLoading = false;
                        this.toast.showTopToast(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyListPage.prototype.openSurveyDetail = function (surveyId) {
        this.navCtrl.push('SurveyInputPage', { id: surveyId });
    };
    SurveyListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-survey-list',template:/*ion-inline-start:"C:\Users\mr k\testpull\ponnymobileedit\src\pages\survey-list\survey-list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Survey List</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-item-sliding *ngFor="let survey of surveys">\n\n      <ion-item (tap)="openSurveyDetail(survey.id)">\n\n        <!-- <ion-avatar item-start>\n\n          <img src="img/slimer.png">\n\n        </ion-avatar> -->\n\n        <h2>{{survey.clientName}}</h2>\n\n        <h3 *ngIf="survey.companyName">{{survey.companyName}}</h3>\n\n        <p>{{survey.applicantAddress}}</p>\n\n        <ion-note item-end>\n\n          {{survey.receiptDate}}\n\n        </ion-note>\n\n      </ion-item>\n\n      <ion-item-options side="left">\n\n        <button ion-button color="primary">\n\n          <ion-icon name="undo"></ion-icon>\n\n          Return\n\n        </button>\n\n        <button ion-button color="secondary">\n\n          <ion-icon name="call"></ion-icon>\n\n          Call\n\n        </button>\n\n      </ion-item-options>\n\n      <ion-item-options side="right">\n\n        <button ion-button color="primary">\n\n          <ion-icon name="mail"></ion-icon>\n\n          Email\n\n        </button>\n\n      </ion-item-options>\n\n    </ion-item-sliding>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mr k\testpull\ponnymobileedit\src\pages\survey-list\survey-list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_survey_service__["a" /* SurveyServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_toast_service__["a" /* ToastServiceProvider */]])
    ], SurveyListPage);
    return SurveyListPage;
}());

//# sourceMappingURL=survey-list.js.map

/***/ })

});
//# sourceMappingURL=0.js.map