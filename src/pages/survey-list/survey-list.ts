import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SurveyServiceProvider } from '../../providers/survey.service';
import { ToastServiceProvider } from '../../providers/toast.service';
import { BaseResponse } from '../../models/base';
import { SurveyListResponse, SurveyList } from '../../models/survey';

@IonicPage()
@Component({
  selector: 'page-survey-list',
  templateUrl: 'survey-list.html'
})
export class SurveyListPage {
  surveys: Array<SurveyList> = [];
  isLoading = true;
  constructor(
    private navCtrl: NavController,
    private survey: SurveyServiceProvider,
    private toast: ToastServiceProvider
  ) {}

  ionViewDidLoad() {}

  async getSurveys() {
    try {
      const response: BaseResponse = await this.survey.getSurveys();
      this.isLoading = false;
      if (!response.isSuccess) {
        this.toast.showTopToast(response.errorMessage);
      } else {
        const surveyList: SurveyListResponse = response.value;
        this.surveys = surveyList.inputOrderList.map((item) => {
          const obj = item;
          const time = new Date(obj.receiptDate);
          time.setUTCHours(time.getUTCHours() + -time.getTimezoneOffset() / 60);
          obj.receiptDate = time.toLocaleString();
          return obj;
        });
      }
    } catch (error) {
      this.isLoading = false;
      this.toast.showTopToast(error);
    }
  }

  openSurveyDetail(surveyId: number) {
    this.navCtrl.push('SurveyInputPage', { id: surveyId });
  }
}
