import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveyMapPage } from './survey-map';

@NgModule({
  declarations: [
    SurveyMapPage,
  ],
  imports: [
    IonicPageModule.forChild(SurveyMapPage),
  ],
})
export class SurveyMapPageModule {}
