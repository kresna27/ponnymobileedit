import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  LocationService,
  MyLocation,
  GeocoderResult,
  LatLng,
  Geocoder
} from '@ionic-native/google-maps';
import { SurveyInputPage } from '../survey-input/survey-input';
/**
 * Generated class for the SurveyMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey-map',
  templateUrl: 'survey-map.html',
})
export class SurveyMapPage {
  map: GoogleMap;
  marker: Marker;
  callback: any;
  @ViewChild("map") mapElement:ElementRef;
  geolocationMaps :any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    this.callback = this.navParams.get("callback")
    this.loadMap();
  }

  done(){
    this.callback(this.geolocationMaps).then(()=>{
      this.navCtrl.pop();
   });
  }

  loadMap() {
    LocationService.getMyLocation().then((myLocation: MyLocation) => {
      let options: GoogleMapOptions = {
        camera: {
          target: myLocation.latLng,
          zoom: 18,
         tilt: 30
        }
      };

    this.map = GoogleMaps.create(this.mapElement.nativeElement, options);

    Geocoder.geocode({
      "position": myLocation.latLng
    }).then((results: GeocoderResult[]) => {
      if (results.length == 0) {
        return null;
      }
      let address: any = [
        results[0].subThoroughfare || "",
        results[0].thoroughfare || "",
        results[0].locality || "",
        results[0].adminArea || "",
        results[0].postalCode || "",
        results[0].country || ""].join(", ");


        this.marker = this.map.addMarkerSync({
          title: address,
          icon: 'blue',
          animation: 'DROP',
          position: myLocation.latLng

        });

        this.marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {

        });
      });

    // this.map.one(GoogleMapsEvent.MAP_READY)
    // .then(() => {
    //   console.log('Map is ready!');

    //   this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
    //       (data) => {

    //         if(this.marker) this.marker.remove();
    //         Geocoder.geocode({
    //           "position": new LatLng(data[0].lat, data[0].lng)
    //         }).then((results: GeocoderResult[]) => {
    //           if (results.length == 0) {
    //             return null;
    //           }
    //           let address: any = [
    //             results[0].subThoroughfare || "",
    //             results[0].thoroughfare || "",
    //             results[0].locality || "",
    //             results[0].adminArea || "",
    //             results[0].postalCode || "",
    //             results[0].country || ""].join(", ");


    //             this.marker = this.map.addMarkerSync({
    //               title: address,
    //               icon: 'blue',
    //               animation: 'DROP',
    //               position: {
    //                 lat: data[0].lat,
    //                 lng: data[0].lng
    //               }

    //             });

    //             this.marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {

    //             });

    //             this.geolocationMaps = {
    //               latitude : data[0].lat,
    //               longitude : data[0].lng,
    //               address : address
    //           }
    //           });


    //       }
    //   );
    // });
    });
  }

}
