import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginRequest } from '../../models/login';
import { NgForm } from '@angular/forms';
import { FormValidatorServiceProvider } from '../../providers/form-validator.service';
import { AuthServiceProvider } from '../../providers/auth.service';
import { ToastServiceProvider } from '../../providers/toast.service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public credentials: LoginRequest;
  @ViewChild('form') form: NgForm;
  constructor(
    public navCtrl: NavController,
    private formValidator: FormValidatorServiceProvider,
    private auth: AuthServiceProvider,
    private toast: ToastServiceProvider
  ) {
    this.credentials = new LoginRequest();
  }

  signIn() {
    this.form.ngSubmit;
    if (this.form.valid) {
      this.auth.authenticate(this.credentials);
    } else {
      const errMsg = this.formValidator.getFirstErrorMessage(this.form);
      this.toast.showTopToast(errMsg);
    }
  }
}
