import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Toast, ActionSheetController, Platform } from 'ionic-angular';
import { SurveyDetailData, SurveyDetailChild } from '../../models/survey';
import { ToastServiceProvider } from '../../providers/toast.service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { SurveyMapPage } from '../survey-map/survey-map';
import { Geolocation } from '@ionic-native/geolocation';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { MapsServiceProvider } from '../../providers/maps.service';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  LocationService,
  MyLocation,
  GeocoderResult,
  LatLng,
  Geocoder
} from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-survey-input',
  templateUrl: 'survey-input.html'
})
export class SurveyInputPage {

  formSurvey ={
    isSuccess: true,
    value: [
      {
        name: 'Hasil Survey Rumah',
        tag: 'HasilSurvey',
        orderIdx: 1,
        details: [
          {
            id: 1,
            type: 'select',
            form: 'AlamatSesuaiId',
            label: 'Alamat Sesuai',
            error: 'Alamat sesuai harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'AlamatSekarang',
              label: 'Alamat Sekarang',
              error: 'Alamat sekarang tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 101,
                label: 'Tidak'
              }
            ],
            value: ''
          },
          {
            id: 2,
            type: 'select',
            form: 'AlamatDitemukanId',
            label: 'Alamat Ditemukan',
            error: 'Alamat Ditemukan harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 2,
                label: 'Tidak'
              }
            ],
            value: ''
          },
          {
            id: 3,
            type: 'select',
            form: 'AlamatDitemukanKantorKosongId',
            label: 'Alamat Ditemukan Tetapi Kantor Kosong/Pindah',
            error: 'Alamat Ditemukan Tetapi Kantor Kosong harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 2,
                label: 'Tidak'
              }
            ],
            value: ''
          },
          {
            id: 4,
            type: 'select',
            form: 'AlamatDitemukanAplikanTidakDikenalId',
            label: 'Alamat Ditemukan Tetapi Aplikan Tidak Dikenal',
            error: 'Alamat Ditemukan Tetapi Aplikan Tidak Dikenal harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 2,
                label: 'Tidak'
              }
            ],
            value: ''
          }
        ]
      },
      {
        name: 'Hasil Wawancara Pihak Pertama (SI 1)',
        tag: 'WawancaraPihak1_Rumah',
        orderIdx: 2,
        details: [
          {
            id: 5,
            type: 'text',
            form: 'Nama',
            label: 'Nama',
            error: 'Nama tidak boleh kosong!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 6,
            type: 'select',
            form: 'JenisKelaminId',
            label: 'Jenis Kelamin',
            error: 'Jenis Kelamin harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Laki-Laki'
              },
              {
                id: 2,
                label: 'Perempuan'
              }
            ],
            value: ''
          },
          {
            id: 7,
            type: 'select',
            form: 'HubunganSumberInformasiId',
            label: 'Hubungan Sumber Informasi',
            error: 'Hubungan SumberInformasi harus dipilih',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'HubunganSumberInformasiLainnya',
              label: null,
              error: 'Hubungan Sumber Informasi Lainnya tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Aplikan'
              },
              {
                id: 2,
                label: 'Suami/Istri'
              },
              {
                id: 3,
                label: 'Anak'
              },
              {
                id: 4,
                label: 'Saudara'
              },
              {
                id: 5,
                label: 'Kakak/Adik'
              },
              {
                id: 6,
                label: 'Orang Tua'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 8,
            type: 'select',
            form: 'LingkunganRumahId',
            label: 'Lingkungan Rumah',
            error: 'Lingkungan Rumah harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Real/Estate'
              },
              {
                id: 2,
                label: 'Ruko/Rukan'
              },
              {
                id: 3,
                label: 'Apartemen'
              },
              {
                id: 4,
                label: 'Perumahan'
              },
              {
                id: 5,
                label: 'Lingkungan Biasa'
              }
            ],
            value: ''
          },
          {
            id: 9,
            type: 'select',
            form: 'TipeTempatTinggalId',
            label: 'Tipe Tempat Tinggal',
            error: 'Tipe Tempat Tinggal harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'TipeTempatTinggalLainnya',
              label: null,
              error: 'Tipe Tempat Tinggal Lainnya tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Rumah'
              },
              {
                id: 2,
                label: 'Apartemen'
              },
              {
                id: 3,
                label: 'Pabrik'
              },
              {
                id: 4,
                label: 'Gudang'
              },
              {
                id: 5,
                label: 'Ruko'
              },
              {
                id: 6,
                label: 'Rumah Susun'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 10,
            type: 'select',
            form: 'LokasiRumahId',
            label: 'Lokasi Rumah',
            error: 'Lokasi Rumah harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Jalan Raya'
              },
              {
                id: 2,
                label: 'Ruko/Rukan'
              },
              {
                id: 3,
                label: 'Kawasan Industri'
              },
              {
                id: 4,
                label: 'Gang, Masuk Mobil'
              },
              {
                id: 5,
                label: 'Gang, Tidak Masuk Mobil'
              }
            ],
            value: ''
          },
          {
            id: 11,
            type: 'select',
            form: 'KondisiJalanId',
            label: 'Kondisi Jalan',
            error: 'Kondisi Jalan harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Aspal/Beton/Permanen'
              },
              {
                id: 2,
                label: 'Tanah/Tidak Permanen'
              }
            ],
            value: ''
          },
          {
            id: 12,
            type: 'select',
            form: 'JenisBangunanId',
            label: 'Jenis Bangunan',
            error: 'Jenis Bangunan harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Permanen'
              },
              {
                id: 2,
                label: 'Semi Permanen'
              },
              {
                id: 3,
                label: 'Non Permanen/Bedeng Triplek'
              }
            ],
            value: ''
          },
          {
            id: 13,
            type: 'inputWithLabel',
            form: null,
            label: 'Jumlah Tanggungan',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'JumlahTanggungan',
              label: 'Orang',
              error: 'Jumlah Tanggungan tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 14,
            type: 'select',
            form: 'StatusPerkawinanId',
            label: 'Status Perkawinan',
            error: 'Status Perkawinan harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Kawin'
              },
              {
                id: 2,
                label: 'Belum Kawin'
              },
              {
                id: 3,
                label: 'Janda/Duda'
              }
            ],
            value: ''
          },
          {
            id: 15,
            type: 'inputWithLabel',
            form: null,
            label: 'Lama Tinggal',
            error: 'Lama Tinggal harus diisi',
            placeholder: null,
            labelDivider: null,
            ext1: {
              type: 'number',
              form: 'LamaTinggalTahun',
              label: 'Tahun',
              error: 'Lama Tinggal Tahun tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: {
              type: 'number',
              form: 'LamaTinggalBulan',
              label: 'Bulan',
              error: 'Lama Tinggal Bulan tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 16,
            type: 'select',
            form: 'AplikanTinggalDisiniId',
            label: 'Aplikan Tinggal Disini',
            error: 'Aplikan Tinggal Disini harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 2,
                label: 'Tidak'
              },
              {
                id: 3,
                label: 'Tidak Tahu'
              }
            ],
            value: ''
          },
          {
            id: 17,
            type: 'number',
            form: 'UsiaAplikan',
            label: 'Usia Aplikan',
            error: 'Usia Aplikan tidak boleh kosong!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 18,
            type: 'select',
            form: 'PendidikanAplikanId',
            label: 'Pendidikan Aplikan',
            error: 'Pendidikan Aplikan harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'SD'
              },
              {
                id: 2,
                label: 'SMP'
              },
              {
                id: 3,
                label: 'SMU'
              },
              {
                id: 4,
                label: 'D3'
              },
              {
                id: 5,
                label: 'S1'
              },
              {
                id: 6,
                label: 'S2'
              },
              {
                id: 7,
                label: 'S3'
              }
            ],
            value: ''
          },
          {
            id: 19,
            type: 'tel',
            form: 'NoHpAplikan',
            label: 'No Hp Aplikan',
            error: 'No Hp Aplikan tidak boleh kosong!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 20,
            type: 'text',
            form: 'NamaKantorAplikan',
            label: 'Nama Kantor Aplikan',
            error: 'Nama Kantor Aplikan tidak boleh kosong!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 21,
            type: 'multiselect',
            form: 'FasilitasRumahId',
            label: 'Fasilitas Rumah',
            error: 'Fasilitas Rumah harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'FasilitasRumahLainnya',
              label: null,
              error: 'Fasilitas Rumah Lainnya tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: {
              type: 'tel',
              form: 'FasilitasRumahNomorTelp',
              label: null,
              error: 'No Telp tidak boleh kosong!',
              placeHolder: 'No Telp',
              value: null
            },
            ext3: null,
            data: [
              {
                id: 102,
                label: 'Telepon'
              },
              {
                id: 1,
                label: 'Taman'
              },
              {
                id: 2,
                label: 'Carport'
              },
              {
                id: 3,
                label: 'Garasi'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 22,
            type: 'select',
            form: 'StatusKepemilikanId',
            label: 'Status Kepemilikan',
            error: 'Status Kepemilikan harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'StatusKepemilikanLainnya',
              label: null,
              error: 'Status Kepemilikan Lainnya tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: {
              type: 'text',
              form: 'StatusKepemilikanBerakhirnyaKontrak',
              label: null,
              error: 'Status KepemilikanBerakhirnya Kontrak tidak boleh kosong!',
              placeHolder: 'Berakhirnya Kontrak',
              value: null
            },
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Milik Sendiri'
              },
              {
                id: 2,
                label: 'Milik Keluarga'
              },
              {
                id: 3,
                label: 'Rumah Dinas'
              },
              {
                id: 102,
                label: 'Sewa/Kost'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 23,
            type: 'inputWithLabel',
            form: null,
            label: 'Jumlah Penghuni',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'JumlahPenghuni',
              label: 'Orang',
              error: 'Jumlah penghuni tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 24,
            type: 'text',
            form: 'NamaIbuKandung',
            label: 'Nama Ibu Kandung',
            error: 'Nama Ibu Kandung tidak boleh kosong!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 25,
            type: 'text',
            form: 'NamaSuamiIstri',
            label: 'Nama Suami/Istri',
            error: 'Nama Suami/Istri tidak boleh kosong!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 26,
            type: 'select',
            form: 'PekerjaanPasanganId',
            label: 'Pekerjaan Pasangan',
            error: 'Pekerjaan Pasangan harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'PekerjaanPasanganLainnya',
              label: null,
              error: 'Pekerjaan Pasangan Lainnya tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Karyawan'
              },
              {
                id: 2,
                label: 'Profesional'
              },
              {
                id: 3,
                label: 'Pensiun'
              },
              {
                id: 4,
                label: 'TNI/Polri'
              },
              {
                id: 5,
                label: 'Wiraswasta'
              },
              {
                id: 6,
                label: 'Pegawai Negeri'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 27,
            type: 'text',
            form: 'NamaKantorPasangan',
            label: 'Nama Kantor Pasangan Aplikan',
            error: 'Nama Kantor Pasangan Aplikan tidak boleh kosong!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 28,
            type: 'select',
            form: 'KapasitasListrikId',
            label: 'Kapasitas Listrik',
            error: 'Kapasitas Listrik harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: '450 Watt'
              },
              {
                id: 2,
                label: '900 Watt'
              },
              {
                id: 3,
                label: '1300 Watt'
              },
              {
                id: 4,
                label: '2200 Watt'
              },
              {
                id: 5,
                label: '2200 Watt'
              }
            ],
            value: ''
          },
          {
            id: 29,
            type: 'select',
            form: 'KepemilikanKartuKreditId',
            label: 'Kepemilikan Kartu Kredit',
            error: 'Kepemilikan Kartu Kredit harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'JenisKartuKredit',
              label: null,
              error: 'Jenis Kartu Kredit tidak boleh kosong!',
              placeHolder: 'Jenis Kartu Kredit',
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 101,
                label: 'Ya'
              },
              {
                id: 1,
                label: 'Tidak'
              }
            ],
            value: ''
          }
        ]
      },
      {
        name: 'Hasil Wawancara Pihak Kedua (SI 2)',
        tag: 'WawancaraPihak2_Rumah',
        orderIdx: 3,
        details: [
          {
            id: 30,
            type: 'text',
            form: 'Nama',
            label: 'Nama',
            error: 'Nama tidak boleh kosong!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 31,
            type: 'select',
            form: 'JenisKelaminId',
            label: 'Jenis Kelamin',
            error: 'Jenis Kelamin harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Laki-Laki'
              },
              {
                id: 2,
                label: 'Perempuan'
              }
            ],
            value: ''
          },
          {
            id: 32,
            type: 'select',
            form: 'HubunganSumberInformasiId',
            label: 'Hubungan Sumber Informasi',
            error: 'Hubungan SumberInformasi harus dipilih',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'HubunganSumberInformasiLainnya',
              label: 'Hubungan Sumber Informasi Lainnya',
              error: 'Hubungan Sumber Informasi Lainnya tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: {
              type: 'text',
              form: 'NamaKetuaRT',
              label: 'No Rumah Tetangga',
              error: 'No Rumah Tetanggatidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext3: {
              type: 'number',
              form: 'NoRumahTetangga',
              label: null,
              error: null,
              placeHolder: null,
              value: null
            },
            data: [
              {
                id: 102,
                label: 'Ketua RT'
              },
              {
                id: 103,
                label: 'Tetangga'
              },
              {
                id: 1,
                label: 'Security Perumahan'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 33,
            type: 'select',
            form: 'AplikanDikenalId',
            label: 'Aplikan Dikenal',
            error: 'Aplikan Dikenal harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KeteranganAplikanDikenal',
              label: 'Keterangan Aplikan',
              error: 'Keterangan Aplikan tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 101,
                label: 'Tidak'
              }
            ],
            value: ''
          },
          {
            id: 34,
            type: 'select',
            form: 'AplikanTinggalDiAlamatTsbId',
            label: 'Aplikan Tinggal Di Alamat Tersebut',
            error: 'Aplikan Tinggal Di Alamat Tersebut harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KeteranganAplikanTinggal',
              label: 'Keterangan Alamat Aplikan',
              error: 'Keterangan Alamat Aplikan tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 2,
                label: 'Tidak'
              },
              {
                id: 101,
                label: 'Tidak Tahu'
              }
            ],
            value: ''
          },
          {
            id: 35,
            type: 'select',
            form: 'StatusKepemilikanId',
            label: 'Status Kepemilikan',
            error: 'Status Kepemilikan harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KeteranganStatusKepemilikan',
              label: 'Keterangan Status Kepemilikan',
              error: 'Keterangan Status Kepemilikan tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Sesuai'
              },
              {
                id: 2,
                label: 'Tidak'
              },
              {
                id: 101,
                label: 'Tidak Tahu'
              }
            ],
            value: ''
          },
          {
            id: 36,
            type: 'select',
            form: 'LamaTinggalId',
            label: 'Lama Tinggal',
            error: 'Lama Tinggal harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KeteranganLamaTinggal',
              label: 'Keterangan Lama Tinggal',
              error: 'Keterangan Lama Tinggal tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Sesuai'
              },
              {
                id: 2,
                label: 'Tidak'
              },
              {
                id: 101,
                label: 'Tidak Tahu'
              }
            ],
            value: ''
          }
        ]
      },
      {
        name: 'Hasil Wawancara Pihak Ketiga (SI 3)',
        tag: 'WawancaraPihak3_Rumah',
        orderIdx: 4,
        details: [
          {
            id: 37,
            type: 'text',
            form: 'Nama',
            label: 'Nama',
            error: 'Nama harus diisi!',
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 38,
            type: 'select',
            form: 'JenisKelaminId',
            label: 'Jenis Kelamin',
            error: 'Jenis Kelamin harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Laki-Laki'
              },
              {
                id: 2,
                label: 'Perempuan '
              }
            ],
            value: ''
          },
          {
            id: 39,
            type: 'select',
            form: 'HubunganSumberInformasiId',
            label: 'Hubungan Sumber Informasi',
            error: 'Hubungan SumberInformasi harus dipilih',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'HubunganSumberInformasiLainnya',
              label: 'Hubungan Sumber Informasi Lainnya',
              error: 'Hubungan sumber informasi lainnya harus diisi!',
              placeHolder: null,
              value: null
            },
            ext2: {
              type: 'text',
              form: 'NamaKetuaRT',
              label: 'Nama Ketua RT',
              error: 'Nama ketua RT harus diisi!',
              placeHolder: null,
              value: null
            },
            ext3: {
              type: 'number',
              form: 'NoRumahTetangga',
              label: 'No Rumah Tetangga',
              error: 'No rumah tetangga harus diisi!',
              placeHolder: null,
              value: null
            },
            data: [
              {
                id: 102,
                label: 'Ketua RT'
              },
              {
                id: 103,
                label: 'Tetangga'
              },
              {
                id: 1,
                label: 'Security Perumahan'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 40,
            type: 'select',
            form: 'AplikanDikenalId',
            label: 'Aplikan Dikenal',
            error: 'Aplikan Dikenal harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'AplikanDikenalKeterangan',
              label: 'Keterangan Aplikan',
              error: 'Ketarangan aplikan harus diisi!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 101,
                label: 'Tidak'
              }
            ],
            value: ''
          },
          {
            id: 41,
            type: 'select',
            form: 'AplikanTinggalDiAlamatTsbId',
            label: 'Aplikan Tinggal Di Alamat Tersebut',
            error: 'Aplikan Tinggal Di Alamat Tersebut harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KeteranganAplikanTinggal',
              label: 'Keterangan Alamat Aplikan',
              error: 'Keterangan alamat aplikan harus diisi!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Ya'
              },
              {
                id: 2,
                label: 'Tidak'
              },
              {
                id: 101,
                label: 'Tidak Tahu'
              }
            ],
            value: ''
          },
          {
            id: 42,
            type: 'select',
            form: 'StatusKepemilikanId',
            label: 'Status Kepemilikan',
            error: 'Status Kepemilikan harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KeteranganStatusKepemilikan',
              label: 'Keterangan Status Kepemilikan',
              error: 'Keterangan status kepemilikan harus diisi!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Sesuai'
              },
              {
                id: 2,
                label: 'Tidak'
              },
              {
                id: 101,
                label: 'Tidak Tahu'
              }
            ],
            value: ''
          },
          {
            id: 43,
            type: 'select',
            form: 'LamaTinggalId',
            label: 'Lama Tinggal',
            error: 'Lama Tinggal harus di pilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KeteranganLamaTinggal',
              label: 'keterangan Lama Tinggal',
              error: 'Keterangan lama tinggal harus diisi!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Sesuai'
              },
              {
                id: 2,
                label: 'Tidak'
              },
              {
                id: 101,
                label: 'Tidak Tahu'
              }
            ],
            value: ''
          }
        ]
      },
      {
        name: 'Hasil Pengamatan Saat Survey',
        tag: 'PengamatanSaatSurvey_Rumah',
        orderIdx: 5,
        details: [
          {
            id: 44,
            type: 'select',
            form: 'KondisiBangunanRumahId',
            label: 'Kondisi Bangunan Rumah',
            error: 'Kondisi bangunan rumah harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Baik/Terawat'
              },
              {
                id: 2,
                label: 'Sedang/Kurang Terawat'
              },
              {
                id: 3,
                label: 'Rusak/Tidak Terawat'
              }
            ],
            value: ''
          },
          {
            id: 45,
            type: 'inputWithLabel',
            form: null,
            label: 'Luas Tanah',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: {
              type: 'number',
              form: 'LuasTanahPanjang',
              label: 'M',
              error: 'Panjang tanah tidak boleh kosong!',
              placeHolder: 'Panjang',
              value: null
            },
            ext2: {
              type: 'number',
              form: 'LuasTanahLebar',
              label: 'M',
              error: 'Lebar tanah tidak boleh kosong!',
              placeHolder: 'Lebar',
              value: null
            },
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 46,
            type: 'inputWithLabel',
            form: null,
            label: 'Luas Bangunan',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: {
              type: 'number',
              form: 'LuasBangunanPanjang',
              label: 'M',
              error: 'Panjang bangunan tidak boleh kosong!',
              placeHolder: 'Panjang',
              value: null
            },
            ext2: {
              type: 'number',
              form: 'LuasBangunanLebar',
              label: 'M',
              error: 'Lebar bangunan tidak boleh kosong!',
              placeHolder: 'Lebar',
              value: null
            },
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 47,
            type: 'select',
            form: 'JumlahTingkatId',
            label: 'Jumlah Tingkat',
            error: 'Jumlah tingkat harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: '1'
              },
              {
                id: 2,
                label: '2'
              },
              {
                id: 3,
                label: '3'
              },
              {
                id: 4,
                label: '>3'
              }
            ],
            value: ''
          },
          {
            id: 48,
            type: 'inputWithLabel',
            form: null,
            label: 'Lebar Jalan',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: {
              type: 'number',
              form: 'LebarJalan',
              label: 'M',
              error: 'Lebar jalan tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 49,
            type: 'inputWithLabel',
            form: null,
            label: 'Jumlah Lantai Rumah',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: {
              type: 'number',
              form: 'JumlahLantaiRumah',
              label: 'Tingkat',
              error: 'Jumlah lantai rumah tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 50,
            type: 'select',
            form: 'KondisiDindingRumahId',
            label: 'Kondisi Dinding Rumah',
            error: 'Kondisi dinding rumah harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KondisiDindingRumahLainnya',
              label: 'Kondisi Dinding Rumah Lainnya',
              error: 'Kondisi dinding rumah lainnya tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Tembok Diplester'
              },
              {
                id: 2,
                label: 'Tembok Tidak Diplester'
              },
              {
                id: 3,
                label: 'Sebagian/Seluruhnya Tidak Disemen'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 51,
            type: 'select',
            form: 'KondisiAtapRumahId',
            label: 'Kondisi Atap Rumah',
            error: 'Kondisi atap rumah harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'text',
              form: 'KondisiAtapRumahLainnya',
              label: 'Kondisi Atap Rumah Lainnya',
              error: 'Kondisi atap rumah lainnya tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Genteng'
              },
              {
                id: 2,
                label: 'Asbes/Seng'
              },
              {
                id: 3,
                label: 'Kayu/Sirap'
              },
              {
                id: 101,
                label: 'Lainnya'
              }
            ],
            value: ''
          },
          {
            id: 52,
            type: 'select',
            form: 'KendaraanYangDimilikiId',
            label: 'Kendaraan Yang Dimiliki',
            error: 'Kendaraan Yang Dimiliki harus dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: {
              type: 'number',
              form: 'JumlahKendaraan',
              label: 'Jumlah Kendaraan',
              error: 'Jumlah kendaraan tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext2: {
              type: 'text',
              form: 'JenisKendaraan',
              label: 'Jenis Kendaraan',
              error: 'Jenis kendaraan tidak boleh kosong!',
              placeHolder: null,
              value: null
            },
            ext3: null,
            data: [
              {
                id: 1012,
                label: 'Ya'
              },
              {
                id: 1,
                label: 'Tidak'
              }
            ],
            value: ''
          },
          {
            id: 53,
            type: 'select',
            form: 'PencarianAlamatId',
            label: 'Pencarian Alamat',
            error: 'Pencarian alamat perlu dipilih!',
            placeholder: 'Pilih...',
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: [
              {
                id: 1,
                label: 'Mudah'
              },
              {
                id: 2,
                label: 'Sulit'
              },
              {
                id: 3,
                label: 'Tidak Terlacak'
              }
            ],
            value: ''
          }
        ]
      },
      {
        name: 'Map Rumah',
        tag: 'SurveyMap',
        orderIdx: 6,
        details: [
          {
            id: 54,
            type: 'map',
            form: null,
            label: 'Map',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: {
              type: 'decimal',
              form: 'Longitude',
              label: 'Longitude',
              error: 'Longitude harus diisi! Klik pada map untuk mengisi.',
              placeHolder: null,
              value: null
            },
            ext2: {
              type: 'decimal',
              form: 'Latitude',
              label: 'Latitude',
              error: 'Latitude harus diisi! Klik pada map untuk mengisi.',
              placeHolder: null,
              value: null
            },
            ext3: null,
            data: null,
            value: ''
          }
        ]
      },
      {
        name: 'Upload Gambar Rumah',
        tag: 'SurveyDocument',
        orderIdx: 7,
        details: [
          {
            id: 55,
            type: 'upload',
            form: 'UploadUrl',
            label: 'Upload',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          }
        ]
      },
      {
        name: 'Surveyor',
        tag: 'SurveyorInfo',
        orderIdx: 8,
        details: [
          {
            id: 56,
            type: 'readOnlyText',
            form: 'Code',
            label: 'Kode',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 57,
            type: 'readOnlyText',
            form: 'Nama',
            label: 'Nama',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 58,
            type: 'readOnlyText',
            form: 'Tanggal',
            label: 'Tanggal',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          },
          {
            id: 59,
            type: 'readOnlyText',
            form: 'Jam',
            label: 'Jam',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          }
        ]
      },
      {
        name: 'Catatan',
        tag: 'SurveyNote',
        orderIdx: 9,
        details: [
          {
            id: 60,
            type: 'textarea',
            form: 'Catatan',
            label: 'Catatan',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          }
        ]
      },
      {
        name: 'Informasi Tambahan',
        tag: 'InformasiTambahan',
        orderIdx: 10,
        details: [
          {
            id: 61,
            type: 'textarea',
            form: 'InformasiTambahan',
            label: 'Informasi Tambahan',
            error: null,
            placeholder: null,
            labelDivider: null,
            ext1: null,
            ext2: null,
            ext3: null,
            data: null,
            value: ''
          }
        ]
      }
    ]
  };

  geolocationMaps: any = {};
  surveyImage: any = [];
  surveyTampil: any = {};
  specialExt: any = [101,102,103,1021];
  extData = {ext1:101,ext2:102,ext3:103,ext12:1012};
  lengthFormSurvey = 0;
  lenghtEachStep = {};
  surveyMultiSelect:any = [];
  surveyMultiSelectId:any = [];
  index = {
    form: 0,
    field: 0
  };
  isExt1 = false;
  isExt2 = false;
  isExt3 = false;

  constructor(private camera: Camera, public navCtrl: NavController, public navParams: NavParams, private toast: ToastServiceProvider, private actionSheetCtrl: ActionSheetController, private geolocation: Geolocation, private photoViewer: PhotoViewer, private mapsServiceProvider:MapsServiceProvider) {


  }

  ionViewDidLoad() {
    this.lengthFormSurvey = this.formSurvey.value.length;
    let index = 0;
    this.formSurvey.value.map((x) => {
      this.lenghtEachStep[index] = x.details.length;
      index++;
    });
    this.tampilkanSurvey();
    // this.geolocationMaps = this.mapsServiceProvider.geolocationMyLocation();
    this.geolocationMyLocation();
  }

  checkExtMultiselect(data: any){
    if(this.specialExt.includes(data.id)){
      return true;
    }else{
      return false
    }
  }

  geolocationSave(){
    this.formSurvey.value[this.index.form].details[this.index.field].ext1.value = this.geolocationMaps.lat;
    this.formSurvey.value[this.index.form].details[this.index.field].ext2.value = this.geolocationMaps.lng;
  }

  geolocationMyLocation(){

    LocationService.getMyLocation().then((myLocation: MyLocation) => {
      Geocoder.geocode({
        "position": myLocation.latLng
      }).then((results: GeocoderResult[]) => {
        if (results.length == 0) {
          return null;
        }
        let address: any = [
          results[0].subThoroughfare || "",
          results[0].thoroughfare || "",
          results[0].locality || "",
          results[0].adminArea || "",
          results[0].postalCode || "",
          results[0].country || ""].join(", ");

          this.geolocationMaps = myLocation.latLng;
          this.geolocationMaps["address"] = address;

        });
    });
  }

  showImage(i: number){
    this.photoViewer.show(this.surveyImage[i]);
  }

  // closeMaps = function(data) {
  //   return new Promise((resolve, reject) => {
  //           resolve(this.geolocationMaps= data);

  //       });
  // }
  openMaps(){
    this.navCtrl.push(SurveyMapPage);
  }


  surveyRemoveImage(i: number){
    this.surveyImage.splice(i,1);
  }



  //modified

  tampilkanSurvey() {
    this.surveyTampil = this.formSurvey.value[this.index.form].details[this.index.field];
    this.surveyTampil["name"] = this.formSurvey.value[this.index.form].name;
  }

  checkExtSelect(data:any){
    this.isExt1 = data.id === this.extData.ext1 || data.id === this.extData.ext12;
    this.isExt2 = data.id === this.extData.ext2 || data.id === this.extData.ext12;
    this.isExt3 = data.id === this.extData.ext3;
  }

  //modified
  select(data: SurveyDetailData) {
    this.formSurvey.value[this.index.form].details[this.index.field].value = data.label;
    this.checkExtSelect(data);
  }

  multiselect(data: SurveyDetailData){
    if(this.surveyMultiSelect.includes(data.label)){

      let index = this.surveyMultiSelect.indexOf(data.label);
      this.surveyMultiSelect.splice(index, 1);
      this.surveyMultiSelectId.splice(index,1);
    }else{
      this.surveyMultiSelect.push(data.label);
      this.surveyMultiSelectId.push(data.id);
    }

    console.log(this.surveyMultiSelect);

    this.formSurvey.value[this.index.form].details[this.index.field].value = this.surveyMultiSelect;
    this.isExt1 = this.surveyMultiSelectId.includes(101) || this.surveyMultiSelectId.includes(1012);
    this.isExt2 = this.surveyMultiSelectId.includes(102) || this.surveyMultiSelectId.includes(1012);
    this.isExt3 = data.id === 103;

    console.log("ext1" + this.isExt1 + "ext2" + this.isExt2 + "ext3" + this.isExt3)

  }

  next() {
    // //modified
    console.log(this.formSurvey.value[this.index.form].details[this.index.field])
    let surveyInput = this.formSurvey.value[this.index.form].details[this.index.field];
    let allowInputWithLabel = false;
    console.log(surveyInput);


    if(surveyInput.type=="map"){
      this.geolocationSave();
    }

    if(this.surveyImage.length < 4 && surveyInput.type == "upload"){
      this.toast.showTopToast(surveyInput.error);
      return;
    }else if(this.surveyImage.length >= 4 && surveyInput.type == "upload" ){
      surveyInput.value = this.surveyImage;
    }

    if (!surveyInput.value) {
      if (this.isExt1 == true || this.isExt2 == true || this.isExt3 == true) {

      } else {
        if (surveyInput.ext1 !== null || surveyInput.ext2 !== null || surveyInput.ext3 !== null) {
          if (surveyInput.ext1 !== null && !surveyInput.ext1.value) {
            this.toast.showTopToast(surveyInput.ext1.error);
            return;
          } else {
            allowInputWithLabel = true;
          }
          if (surveyInput.ext2 !== null && !surveyInput.ext2.value) {
            this.toast.showTopToast(surveyInput.ext2.error);
            return;
          } else {
            allowInputWithLabel = true;
          }
          if (surveyInput.ext3 !== null && !surveyInput.ext3.value) {
            this.toast.showTopToast(surveyInput.ext3.error);
            return;
          } else {
            allowInputWithLabel = true;
          }
        } else {
          if(this.formSurvey.value[this.index.form].tag == "SurveyorInfo" || this.formSurvey.value[this.index.form].tag == "SurveyNote" || this.formSurvey.value[this.index.form].tag == "InformasiTambahan"){

          }else{
          this.toast.showTopToast(surveyInput.error);
          return;
          }
        }
      }
    }

    if (this.isExt1 == true && !surveyInput.ext1.value) {
      this.toast.showTopToast(surveyInput.ext1.error);
      return;
    } else if (allowInputWithLabel == false && this.isExt1 == false && surveyInput.ext1 !== null) {
      surveyInput.ext1.value = null;
    }

    if (this.isExt2 == true && !surveyInput.ext2.value) {
      this.toast.showTopToast(surveyInput.ext2.error);
      return;
    } else if (allowInputWithLabel == false && this.isExt2 == false && surveyInput.ext2 !== null) {
      surveyInput.ext2.value = null;
    }

    if (this.isExt3 == true && !surveyInput.ext3.value) {
      this.toast.showTopToast(surveyInput.ext3.error);
      return;
    } else if (allowInputWithLabel == false && this.isExt3 == false && surveyInput.ext3 !== null) {
      surveyInput.ext3.value = null;
    }


    if (this.index.field + 1 >= this.lenghtEachStep[this.index.form] && this.index.form + 1 >= this.lengthFormSurvey)
      alert("Survey Complete! Terima Kasih");

    if (this.index.field + 1 >= this.lenghtEachStep[this.index.form]) {
      this.index.form++;
      this.index.field = 0;
    } else {
      this.index.field++;
    }

    this.isExt1 = false;
    this.isExt2 = false;
    this.isExt3 = false;
    this.tampilkanSurvey();

  }

  prev() {

    if (this.index.field <= 0 && this.index.form <= 0) return;

    if (this.index.field <= 0) {
      this.index.form--;
      //modified
      this.index.field = this.formSurvey.value[this.index.form].details.length - 1;
    } else {
      this.index.field--;
    }
    let surveyInput = this.formSurvey.value[this.index.form].details[this.index.field];
    this.isExt1 = surveyInput.ext1 !== null && surveyInput.ext1.value;
    this.isExt2 = surveyInput.ext2 !== null && surveyInput.ext2.value;
    this.isExt3 = surveyInput.ext3 !== null && surveyInput.ext3.value;


    this.tampilkanSurvey();
  }

  ambilGambar() {
    let actionsheet = this.actionSheetCtrl.create({
      title: "Pilih Gambar",
      buttons: [{
        text: "Dari Device",
        handler: () => {
          const options: CameraOptions = {
            quality: 70,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false
          }

          this.camera.getPicture(options).then((imageData) => {
            let dataCamera = 'data:image/jpeg;base64,' + imageData;
            this.surveyImage.push(dataCamera);
          }, (err) => {
            // Handle error
          });
        }
      },
      {
        text: "Dari Kamera",
        handler: () => {
          const options: CameraOptions = {
            quality: 70,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
          }

          this.camera.getPicture(options).then((imageData) => {
            let dataCamera = 'data:image/jpeg;base64,' + imageData;
            this.surveyImage.push(dataCamera);
          }, (err) => {
            // Handle error
          });
        }
      }, {
        text: "Cancel",
        role: "cancel"
      }]
    });

    actionsheet.present();
  }

  uploadGambar() {
    alert("upload gambar");
    //to api gambar
  }
}
