import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveyInputPage } from './survey-input';

@NgModule({
  declarations: [
    SurveyInputPage,
  ],
  imports: [
    IonicPageModule.forChild(SurveyInputPage),
  ],
})
export class SurveyInputPageModule {}
