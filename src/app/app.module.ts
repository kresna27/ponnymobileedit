import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthServiceProvider } from '../providers/auth.service';
import { FormValidatorServiceProvider } from '../providers/form-validator.service';
import { ToastServiceProvider } from '../providers/toast.service';
import { SurveyServiceProvider } from '../providers/survey.service';
import { PrincipalServiceProvider } from '../providers/principal.service';
import { LoadingServiceProvider } from '../providers/loading.service';
import { ApiServiceProvider } from '../providers/api.service';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { Camera } from '@ionic-native/camera';
import { GoogleMaps, GoogleMap } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { SurveyMapPage } from '../pages/survey-map/survey-map';
import { MapsServiceProvider } from '../providers/maps.service';
import { PhotoViewer } from '@ionic-native/photo-viewer';
// import { IonicImageViewerModule } from 'ionic-img-viewer';
@NgModule({
  declarations: [ MyApp, HomePage,SurveyMapPage ],
  imports: [ BrowserModule, IonicModule.forRoot(MyApp), HttpClientModule ],
  bootstrap: [ IonicApp ],
  entryComponents: [ MyApp, HomePage,SurveyMapPage ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthServiceProvider,
    FormValidatorServiceProvider,
    ToastServiceProvider,
    PrincipalServiceProvider,
    LoadingServiceProvider,
    SurveyServiceProvider,
    ApiServiceProvider,
    Camera,
    GoogleMaps,
    Geolocation,
    MapsServiceProvider,
    PhotoViewer
  ]
})
export class AppModule {}
