import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AuthServiceProvider } from '../providers/auth.service';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any ;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private authService: AuthServiceProvider
  ) {
    this.platform.ready().then(() => {
      this.rootPage = 'SurveyInputPage';
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();

      this.authCheck();
    });
  }

  private authCheck() {
    if (window['IonicDevServer']) {
      const routeUrl = document.URL.split('/#/');
      if (routeUrl.length > 1) window.location.href = routeUrl[0];
    }
    this.authService.authNotifier.filter((res) => res !== null).subscribe((res) => {
      if (res) {
        this.rootPage = 'SurveyListPage';
      } else {
        this.rootPage = 'LoginPage';
      }
      this.splashScreen.hide();
    });
  }
}
