class Account {
  LOGIN: string;
  AUTHENTICATE: string;
}

class Survey {
  LISTSURVEY: string;
}

export class EndPoint {
  public static ACCOUNTS: Account = {
    LOGIN: '/accounts/login',
    AUTHENTICATE: '/accounts/authenticate'
  };

  public static SURVEYS: Survey = {
    LISTSURVEY: '/inputorder'
  };
}
