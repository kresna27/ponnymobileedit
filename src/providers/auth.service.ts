import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { ApiServiceProvider } from './api.service';
import { LoadingServiceProvider } from './loading.service';
import { LoginResponse, LoginRequest } from '../models/login';
import { ToastServiceProvider } from './toast.service';
import { BaseResponse } from '../models/base';
import { PrincipalServiceProvider } from './principal.service';
import { EndPoint } from '../config/endpoint';

@Injectable()
export class AuthServiceProvider {
  public authNotifier: BehaviorSubject<any> = new BehaviorSubject(null);
  public isLogin = false;

  constructor(
    public apiService: ApiServiceProvider,
    private loading: LoadingServiceProvider,
    private toast: ToastServiceProvider,
    private principal: PrincipalServiceProvider
  ) {}

  tokenChecker() {
    const token = localStorage.getItem('token');
    /**
     * TODO: Check token validity
     */
    if (token) {
      this.reauthenticate(token);
    } else {
      this.authNotifier.next(false);
    }
  }

  async reauthenticate(token: string) {
    const tempUserData: LoginResponse = new LoginResponse();
    tempUserData.token = token;
    try {
      const response: BaseResponse = await this.apiService.post(EndPoint.ACCOUNTS.AUTHENTICATE, {}).toPromise();
      if (!response.isSuccess) {
        this.toast.showTopToast(response.errorMessage);
      } else {
        this.principal.setUser(response.value);
        this.authNotifier.next(true);
      }
    } catch (error) {
      this.toast.showTopToast(error);
    }
  }

  async authenticate(credentials: LoginRequest) {
    this.loading.startLoading();
    try {
      const response: BaseResponse = await this.apiService.post(EndPoint.ACCOUNTS.LOGIN, credentials).toPromise();
      if (!response.isSuccess) {
        this.toast.showTopToast(response.errorMessage);
      } else {
        this.loading.stopLoading();
        this.principal.setUser(response.value);
        this.authNotifier.next(true);
      }
    } catch (error) {
      this.toast.showTopToast(error);
    }
  }

  logout() {}
}
