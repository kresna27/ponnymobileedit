import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { Config } from '../config/config';
import { PrincipalServiceProvider } from './principal.service';

@Injectable()
export class ApiServiceProvider {
  private url: string;
  private networkIsConnected = true;

  constructor(private http: HttpClient, private principal: PrincipalServiceProvider) {
    this.url = Config.API_URL;

    /**
     * Network Checker
     *
     
    if (this.network.type === 'none' || this.network.type === 'unknown') this.networkIsConnected = false;
      
    this.network.onDisconnect().subscribe(() => {
      this.networkIsConnected = false;
    });
      
    this.network.onConnect().subscribe(() => {
      this.networkIsConnected = true;
    });
      
    if (!window['cordova']) this.networkIsConnected = true;
    
    */
  }

  getUrl() {
    return window['IonicDevServer'] ? 'api' : this.url;
  }

  get(endpoint: string, httpOptions?: any, isJson = true) {
    // if (!this.networkIsConnected) {
    //   return new ErrorObservable('Network error, make sure there are internet connection.');
    // }
    const options = this.createRequestHeader(httpOptions ? httpOptions : {}, isJson);
    return this.http.get(this.getUrl() + endpoint, options).pipe(catchError(this.handleError));
  }

  post(endpoint: string, body: any, httpOptions?: any, isJson = true) {
    // if (!this.networkIsConnected) {
    //   return new ErrorObservable('Network error, make sure there are internet connection.');
    // }
    const options = this.createRequestHeader(httpOptions ? httpOptions : {}, isJson);
    return this.http.post(this.getUrl() + endpoint, body, options).pipe(catchError(this.handleError));
  }

  delete(endpoint: string, httpOptions?: any) {
    // if (!this.networkIsConnected) {
    //   return new ErrorObservable('Network error, make sure there are internet connection.');
    // }
    const options = this.createRequestHeader(httpOptions ? httpOptions : {});
    return this.http.delete(this.getUrl() + endpoint, options).pipe(catchError(this.handleError));
  }

  private createRequestHeader(options?: any, isJson = true): any {
    const headers = options.hasOwnProperty('headers') ? options.headers : {};
    if (isJson) {
      headers['Content-Type'] = 'application/json';
    }
    if (this.principal.getToken()) {
      headers['Authorization'] = `Bearer ${this.principal.getToken()}`;
    }
    headers['Access-Control-Allow-Origin'] = '*';
    options.headers = headers;
    return options;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.hasOwnProperty('error')) {
      if (error.error instanceof ErrorEvent) {
        return new ErrorObservable(error.error.message);
      } else return new ErrorObservable(error.status.toString() + ': ' + error.statusText);
    } else return new ErrorObservable(`Terjadi kesalahan pada sistem.`);
  }
}
