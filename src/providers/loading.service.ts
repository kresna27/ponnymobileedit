import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';

@Injectable()
export class LoadingServiceProvider {
  loading: Loading;
  constructor(public loadingCtrl: LoadingController) {}

  startLoading(label = '', spinner = 'crescent') {
    this.loading = this.loadingCtrl.create({
      content: label,
      spinner: spinner
    });
    this.loading.present();
  }

  stopLoading() {
    this.loading.dismissAll();
  }
}
