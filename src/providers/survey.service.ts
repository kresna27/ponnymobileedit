import { Injectable } from '@angular/core';
import { ApiServiceProvider } from './api.service';
import { EndPoint } from '../config/endpoint';

@Injectable()
export class SurveyServiceProvider {
  constructor(private apiService: ApiServiceProvider) {}

  surveyData:any=[]
  surveyShowData:any={};
  extData = {ext1:101,ext2:102,ext3:103,ext12:1012};
  isExt1 = false;
  isExt2 = false;
  isExt3 = false;


  getSurveys() {
    return this.apiService.get(EndPoint.SURVEYS.LISTSURVEY).toPromise();
  }

  nextSurvey(data: any){

  }

  prevSurvey(data: any){

  }

  checkSurvey(data:any){

  }

  checkExtSelect(data:any){
    switch(data.id){
      case this.extData.ext1 :{
        this.isExt1 = true;
        break;
      }
      case this.extData.ext2 :{
        this.isExt2 = true;
        break;
      }
      case this.extData.ext3 :{
        this.isExt3 = true;
        break;
      }
      case this.extData.ext12 :{
        this.isExt1 = true;
        this.isExt2 = true;
        break;
      }
    }
  }

  selectSurvey(data:any){
    this.surveyShowData = data.label;
    this.checkExtSelect(data);
  }

  multiselectSurvey(data:any){

  }

  surveyShow(data: any){
    this.surveyShowData = data;
  }

}
