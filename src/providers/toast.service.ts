import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastServiceProvider {
  constructor(private toastCtrl: ToastController) {}

  showTopToast(content: string, duration = 5000, closeButton = true, closeButtonText = 'x'): void {
    const toast = this.toastCtrl.create({
      message: content,
      position: 'top',
      duration: duration,
      showCloseButton: closeButton,
      closeButtonText: closeButtonText
    });
    toast.present();
  }
}
