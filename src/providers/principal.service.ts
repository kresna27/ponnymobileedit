import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginResponse } from '../models/login';

@Injectable()
export class PrincipalServiceProvider {
  private isLogin = false;
  private userInfo: LoginResponse;

  constructor() {
    this.userInfo = new LoginResponse();
  }

  setUser(user: LoginResponse) {
    const date = new Date(user.expiration).getTime().toString();
    localStorage.setItem('token', user.token);
    localStorage.setItem('expired', date);
    this.userInfo = user;
    this.isLogin = true;
  }

  getToken(): string {
    return this.userInfo.token;
  }

  getLoginStatus(): boolean {
    return this.isLogin;
  }
}
