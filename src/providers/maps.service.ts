import { Injectable } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  LocationService,
  MyLocation,
  GeocoderResult,
  LatLng,
  Geocoder
} from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
export class MapsServiceProvider {
  constructor(private geolocation: Geolocation) {}

  geolocationMyLocation(){
    let location:any={};
    LocationService.getMyLocation().then((myLocation: MyLocation) => {
      Geocoder.geocode({
        "position": myLocation.latLng
      }).then((results: GeocoderResult[]) => {
        if (results.length == 0) {
          return null;
        }
        let address: any = [
          results[0].subThoroughfare || "",
          results[0].thoroughfare || "",
          results[0].locality || "",
          results[0].adminArea || "",
          results[0].postalCode || "",
          results[0].country || ""].join(", ");

          location = myLocation.latLng;
          location["address"] = address;
          console.log(location);
          return location;
        });
    });
  }

}
