export class SurveyListResponse {
  inputOrderList: Array<SurveyList>;
}

export class SurveyList {
  id: number;
  clientName: string;
  branch: string;
  receiptDate: string;
  applicantName: string;
  applicantAddress: string;
  applicantRegion: string;
  aplicantPhoneNumber: string;
  serviceType: string;
  companyName: string;
  state: string;
  surveyorName: string;
}

export class SurveyDetailExtend {
  type: string;
  form: string;
  label: string;
  error: string;
  placeholder: string;
  value: any;
  data: Array<SurveyDetailData>;
}

export class SurveyDetail {
  name: string;
  tag: string;
  details: Array<SurveyDetailChild>;
}

export class SurveyDetailData {
  id: number;
  label: string;
}

export class SurveyDetailChild {
  id: number;
  type: string;
  form: string;
  label: string;
  error: string;
  placeholder: string;
  labelDivider: string;
  ext1: SurveyDetailExtend;
  ext2: SurveyDetailExtend;
  ext3: SurveyDetailExtend;
  data: Array<SurveyDetailData>;
  value: any;
}

export class temp {
  value: [
    {
      'name': 'Hasil Survey Rumah';
      'tag': 'InputSurveyStep1';
      'details': [
        {
          'id': 1;
          'type': 'select';
          'form': 'AlamatSesuai';
          'label': 'Alamat Sesuai';
          'error': 'Alamat sesuai harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'AlamatSekarang';
            'label': 'Alamat Sekarang';
            'error': 'Alamat sekarang tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            },
            {
              'id': 99;
              'label': 'Tidak';
            }
          ];
          'value': '';
        },
        {
          'id': 2;
          'type': 'select';
          'form': 'AlamatDitemukan';
          'label': 'Alamat Ditemukan';
          'error': 'Alamat Ditemukan harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            }
          ];
          'value': '';
        },
        {
          'id': 3;
          'type': 'select';
          'form': 'AlamatDitemukanKantorKosong';
          'label': 'Alamat Ditemukan Tetapi Kantor Kosong/Pindah';
          'error': 'Alamat Ditemukan Tetapi Kantor Kosong harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            },
            {
              'id': 2;
              'label': 'Tidak';
            }
          ];
          'value': '';
        },
        {
          'id': 4;
          'type': 'select';
          'form': 'AlamatDitemukanAplikanTidakDikenal';
          'label': 'Alamat Ditemukan Tetapi Aplikan Tidak Dikenal';
          'error': 'Alamat Ditemukan Tetapi Aplikan Tidak Dikenal harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            },
            {
              'id': 2;
              'label': 'Tidak';
            }
          ];
          'value': '';
        }
      ];
    },
    {
      'name': 'Informasi Tambahan';
      'tag': 'InputSurveyStep10';
      'details': [
        {
          'id': 61;
          'type': 'textarea';
          'form': 'InformasiTambahan';
          'label': 'Informasi Tambahan';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        }
      ];
    },
    {
      'name': 'Hasil Wawancara Pihak Pertama (SI 1)';
      'tag': 'InputSurveyStep2';
      'details': [
        {
          'id': 5;
          'type': 'text';
          'form': 'Nama';
          'label': 'Nama';
          'error': 'Nama tidak boleh kosong!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 6;
          'type': 'select';
          'form': 'JenisKelamin';
          'label': 'Jenis Kelamin';
          'error': 'Jenis Kelamin harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 7;
          'type': 'select';
          'form': 'HubunganSumberInformasi';
          'label': 'Hubungan Sumber Informasi';
          'error': 'Hubungan SumberInformasi harus dipilih';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'HubunganSumberInformasiLainnya';
            'label': null;
            'error': 'Hubungan Sumber Informasi Lainnya tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Aplikan';
            },
            {
              'id': 2;
              'label': 'Suami/Istri';
            },
            {
              'id': 3;
              'label': 'Anak';
            },
            {
              'id': 4;
              'label': 'Saudara';
            },
            {
              'id': 5;
              'label': 'Kakak/Adik';
            },
            {
              'id': 6;
              'label': 'Orang Tua';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 8;
          'type': 'select';
          'form': 'LingkunganRumah';
          'label': 'Lingkungan Rumah';
          'error': 'Lingkungan Rumah harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Real/Estate';
            },
            {
              'id': 2;
              'label': 'Ruko/Rukan';
            },
            {
              'id': 3;
              'label': 'Apartemen';
            },
            {
              'id': 4;
              'label': 'Perumahan';
            },
            {
              'id': 5;
              'label': 'Lingkungan Biasa';
            }
          ];
          'value': '';
        },
        {
          'id': 9;
          'type': 'select';
          'form': 'TipeTempatTinggal';
          'label': 'Tipe Tempat Tinggal';
          'error': 'Tipe Tempat Tinggal harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'TipeTempatTinggalLainnya';
            'label': null;
            'error': 'Tipe Tempat Tinggal Lainnya tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Rumah';
            },
            {
              'id': 2;
              'label': 'Apartemen';
            },
            {
              'id': 3;
              'label': 'Pabrik';
            },
            {
              'id': 4;
              'label': 'Gudang';
            },
            {
              'id': 5;
              'label': 'Ruko';
            },
            {
              'id': 6;
              'label': 'Rumah Susun';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 10;
          'type': 'select';
          'form': 'LokasiRumah';
          'label': 'Lokasi Rumah';
          'error': 'Lokasi Rumah harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Jalan Raya';
            },
            {
              'id': 2;
              'label': 'Ruko/Rukan';
            },
            {
              'id': 3;
              'label': 'Kawasan Industri';
            },
            {
              'id': 4;
              'label': 'Gang, Masuk Mobil';
            },
            {
              'id': 5;
              'label': 'Gang, Tidak Masuk Mobil';
            }
          ];
          'value': '';
        },
        {
          'id': 11;
          'type': 'select';
          'form': 'KondisiJalan';
          'label': 'Kondisi Jalan';
          'error': 'Kondisi Jalan harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Aspal/Beton/Permanen';
            },
            {
              'id': 2;
              'label': 'Tanah/Tidak Permanen';
            }
          ];
          'value': '';
        },
        {
          'id': 12;
          'type': 'select';
          'form': 'JenisBangunan';
          'label': 'Jenis Bangunan';
          'error': 'Jenis Bangunan harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Permanen';
            },
            {
              'id': 2;
              'label': 'Semi Permanen';
            },
            {
              'id': 3;
              'label': 'Non Permanen/Bedeng Triplek';
            }
          ];
          'value': '';
        },
        {
          'id': 13;
          'type': 'inputWithLabel';
          'form': null;
          'label': 'Jumlah Tanggungan';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'JumlahTanggungan';
            'label': 'Orang';
            'error': 'Jumlah Tanggungan tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 14;
          'type': 'select';
          'form': 'StatusPerkawinan';
          'label': 'Status Perkawinan';
          'error': 'Status Perkawinan harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Kawin';
            },
            {
              'id': 2;
              'label': 'Belum Kawin';
            },
            {
              'id': 3;
              'label': 'Janda/Duda';
            }
          ];
          'value': '';
        },
        {
          'id': 15;
          'type': 'inputWithLabel';
          'form': null;
          'label': 'Lama Tinggal';
          'error': 'Lama Tinggal harus diisi';
          'placeholder': null;
          'labelDivider': null;
          'ext1': {
            'type': 'number';
            'form': 'LamaTinggalTahun';
            'label': 'Tahun';
            'error': 'Lama Tinggal Tahun tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': {
            'type': 'number';
            'form': 'LamaTinggalBulan';
            'label': 'Bulan';
            'error': 'Lama Tinggal Bulan tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 16;
          'type': 'select';
          'form': 'AplikanTinggalDisini';
          'label': 'Aplikan Tinggal Disini';
          'error': 'Aplikan Tinggal Disini harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            },
            {
              'id': 2;
              'label': 'Tidak';
            },
            {
              'id': 3;
              'label': 'Tidak Tahu';
            }
          ];
          'value': '';
        },
        {
          'id': 17;
          'type': 'number';
          'form': 'UsiaAplikan';
          'label': 'Usia Aplikan';
          'error': 'Usia Aplikan tidak boleh kosong!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 18;
          'type': 'select';
          'form': 'PendidikanAplikan';
          'label': 'Pendidikan Aplikan';
          'error': 'Pendidikan Aplikan harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'SD';
            },
            {
              'id': 2;
              'label': 'SMP';
            },
            {
              'id': 3;
              'label': 'SMU';
            },
            {
              'id': 4;
              'label': 'D3';
            },
            {
              'id': 5;
              'label': 'S1';
            },
            {
              'id': 6;
              'label': 'S2';
            },
            {
              'id': 7;
              'label': 'S3';
            }
          ];
          'value': '';
        },
        {
          'id': 19;
          'type': 'tel';
          'form': 'NoHpAplikan';
          'label': 'No Hp Aplikan';
          'error': 'No Hp Aplikan tidak boleh kosong!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 20;
          'type': 'text';
          'form': 'NamaKantorAplikan';
          'label': 'Nama Kantor Aplikan';
          'error': 'Nama Kantor Aplikan tidak boleh kosong!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 21;
          'type': 'select';
          'form': 'FasilitasRumah';
          'label': 'Fasilitas Rumah';
          'error': 'Fasilitas Rumah harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'FasilitasRumahLainnya';
            'label': null;
            'error': 'Fasilitas Rumah Lainnya tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': {
            'type': 'tel';
            'form': 'FasilitasRumahNomorTelp';
            'label': null;
            'error': 'No Telp tidak boleh kosong!';
            'placeHolder': 'No Telp';
            'value': null;
          };
          'ext3': null;
          'data': [
            {
              'id': 100;
              'label': 'Telepon';
            },
            {
              'id': 1;
              'label': 'Taman';
            },
            {
              'id': 2;
              'label': 'Carport';
            },
            {
              'id': 3;
              'label': 'Garasi';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 22;
          'type': 'select';
          'form': 'StatusKepemilikan';
          'label': 'Status Kepemilikan';
          'error': 'Status Kepemilikan harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'StatusKepemilikanLainnya';
            'label': null;
            'error': 'Status Kepemilikan Lainnya tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': {
            'type': 'text';
            'form': 'StatusKepemilikanBerakhirnyaKontrak';
            'label': null;
            'error': 'Status KepemilikanBerakhirnya Kontrak tidak boleh kosong!';
            'placeHolder': 'Berakhirnya Kontrak';
            'value': null;
          };
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Milik Sendiri';
            },
            {
              'id': 2;
              'label': 'Milik Keluarga';
            },
            {
              'id': 3;
              'label': 'Rumah Dinas';
            },
            {
              'id': 100;
              'label': 'Sewa/Kost';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 23;
          'type': 'inputWithLabel';
          'form': null;
          'label': 'Jumlah Penghuni';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'JumlahPenghuni';
            'label': 'Orang';
            'error': 'Jumlah penghuni tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 24;
          'type': 'text';
          'form': 'NamaIbuKandung';
          'label': 'Nama Ibu Kandung';
          'error': 'Nama Ibu Kandung tidak boleh kosong!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 25;
          'type': 'text';
          'form': 'NamaSuamiIstri';
          'label': 'Nama Suami/Istri';
          'error': 'Nama Suami/Istri tidak boleh kosong!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 26;
          'type': 'select';
          'form': 'PekerjaanPasangan';
          'label': 'Pekerjaan Pasangan';
          'error': 'Pekerjaan Pasangan harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'PekerjaanPasanganLainnya';
            'label': null;
            'error': 'Pekerjaan Pasangan Lainnya tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Karyawan';
            },
            {
              'id': 2;
              'label': 'Profesional';
            },
            {
              'id': 3;
              'label': 'Pensiun';
            },
            {
              'id': 4;
              'label': 'TNI/Polri';
            },
            {
              'id': 5;
              'label': 'Wiraswasta';
            },
            {
              'id': 6;
              'label': 'Pegawai Negeri';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 27;
          'type': 'text';
          'form': 'NamaKantorPasangan';
          'label': 'Nama Kantor Pasangan Aplikan';
          'error': 'Nama Kantor Pasangan Aplikan tidak boleh kosong!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 28;
          'type': 'select';
          'form': 'kapasitasListrik';
          'label': 'Kapasitas Listrik';
          'error': 'Kapasitas Listrik harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 29;
          'type': 'select';
          'form': 'KepemilikanKartuKredit';
          'label': 'Kepemilikan Kartu Kredit';
          'error': 'Kepemilikan Kartu Kredit harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'JenisKartuKredit';
            'label': null;
            'error': 'Jenis Kartu Kredit tidak boleh kosong!';
            'placeHolder': 'Jenis Kartu Kredit';
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 100;
              'label': 'Ya';
            },
            {
              'id': 1;
              'label': 'Tidak';
            }
          ];
          'value': '';
        }
      ];
    },
    {
      'name': 'Hasil Wawancara Pihak Kedua (SI 2)';
      'tag': 'InputSurveyStep3';
      'details': [
        {
          'id': 30;
          'type': 'text';
          'form': 'Nama';
          'label': 'Nama';
          'error': 'Nama tidak boleh kosong!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 31;
          'type': 'select';
          'form': 'JenisKelamin';
          'label': 'Jenis Kelamin';
          'error': 'Jenis Kelamin harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Laki-Laki';
            }
          ];
          'value': '';
        },
        {
          'id': 32;
          'type': 'select';
          'form': 'HubunganSumberInformasi';
          'label': 'Hubungan Sumber Informasi';
          'error': 'Hubungan SumberInformasi harus dipilih';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'HubunganSumberInformasiLainnya';
            'label': 'Hubungan Sumber Informasi Lainnya';
            'error': 'Hubungan Sumber Informasi Lainnya tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': {
            'type': 'text';
            'form': 'NamaKetuaRT';
            'label': 'No Rumah Tetangga';
            'error': 'No Rumah Tetanggatidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext3': {
            'type': 'number';
            'form': 'NoRumahTetangga';
            'label': null;
            'error': null;
            'placeHolder': null;
            'value': null;
          };
          'data': [
            {
              'id': 100;
              'label': 'Ketua RT';
            },
            {
              'id': 101;
              'label': 'Tetangga';
            },
            {
              'id': 1;
              'label': 'Security Perumahan';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 33;
          'type': 'select';
          'form': 'AplikanDikenal';
          'label': 'Aplikan Dikenal';
          'error': 'Aplikan Dikenal harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'KeteranganAplikanDikenal';
            'label': 'Keterangan Aplikan';
            'error': 'Keterangan Aplikan tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            },
            {
              'id': 99;
              'label': 'Tidak';
            }
          ];
          'value': '';
        },
        {
          'id': 34;
          'type': 'select';
          'form': 'AplikanTinggalDiAlamatTsb';
          'label': 'Aplikan Tinggal Di Alamat Tersebut';
          'error': 'Aplikan Tinggal Di Alamat Tersebut harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'KeteranganAplikanTinggal';
            'label': 'Keterangan Alamat Aplikan';
            'error': 'Keterangan Alamat Aplikan tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            },
            {
              'id': 2;
              'label': 'Tidak';
            },
            {
              'id': 99;
              'label': 'Tidak Tahu';
            }
          ];
          'value': '';
        },
        {
          'id': 35;
          'type': 'select';
          'form': 'StatusKepemilikan';
          'label': 'Status Kepemilikan';
          'error': 'Status Kepemilikan harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'KeteranganStatusKepemilikan';
            'label': 'Keterangan Status Kepemilikan';
            'error': 'Keterangan Status Kepemilikan tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Sesuai';
            },
            {
              'id': 2;
              'label': 'Tidak';
            },
            {
              'id': 99;
              'label': 'Tidak Tahu';
            }
          ];
          'value': '';
        },
        {
          'id': 36;
          'type': 'select';
          'form': 'LamaTinggal';
          'label': 'Lama Tinggal';
          'error': 'Lama Tinggal harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'KeteranganLamaTinggal';
            'label': 'Keterangan Lama Tinggal';
            'error': 'Keterangan Lama Tinggal tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Sesuai';
            },
            {
              'id': 2;
              'label': 'Tidak';
            },
            {
              'id': 99;
              'label': 'Tidak Tahu';
            }
          ];
          'value': '';
        }
      ];
    },
    {
      'name': 'Hasil Wawancara Pihak Ketiga (SI 3)';
      'tag': 'InputSurveyStep4';
      'details': [
        {
          'id': 37;
          'type': 'text';
          'form': 'Nama';
          'label': 'Nama';
          'error': 'Nama harus diisi!';
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 38;
          'type': 'select';
          'form': 'JenisKelamin';
          'label': 'Jenis Kelamin';
          'error': 'Jenis Kelamin harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Laki-Laki';
            },
            {
              'id': 2;
              'label': 'Perempuan ';
            }
          ];
          'value': '';
        },
        {
          'id': 39;
          'type': 'select';
          'form': 'HubunganSumberInformasi';
          'label': 'Hubungan Sumber Informasi';
          'error': 'Hubungan SumberInformasi harus dipilih';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'HubunganSumberInformasiLainnya';
            'label': 'Hubungan Sumber Informasi Lainnya';
            'error': 'Hubungan sumber informasi lainnya harus diisi!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': {
            'type': 'text';
            'form': 'NamaKetuaRT';
            'label': 'Nama Ketua RT';
            'error': 'Nama ketua RT harus diisi!';
            'placeHolder': null;
            'value': null;
          };
          'ext3': {
            'type': 'number';
            'form': 'NoRumahTetangga';
            'label': 'No Rumah Tetangga';
            'error': 'No rumah tetangga harus diisi!';
            'placeHolder': null;
            'value': null;
          };
          'data': [
            {
              'id': 100;
              'label': 'Ketua RT';
            },
            {
              'id': 101;
              'label': 'Tetangga';
            },
            {
              'id': 1;
              'label': 'Security Perumahan';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 40;
          'type': 'select';
          'form': 'AplikanDikenal';
          'label': 'Aplikan Dikenal';
          'error': 'Aplikan Dikenal harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'AplikanDikenalKeterangan';
            'label': 'Keterangan Aplikan';
            'error': 'Ketarangan aplikan harus diisi!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            },
            {
              'id': 99;
              'label': 'Tidak';
            }
          ];
          'value': '';
        },
        {
          'id': 41;
          'type': 'select';
          'form': 'AplikanTinggalDiAlamatTsb';
          'label': 'Aplikan Tinggal Di Alamat Tersebut';
          'error': 'Aplikan Tinggal Di Alamat Tersebut harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'keteranganAplikanTinggal';
            'label': 'Keterangan Alamat Aplikan';
            'error': 'Keterangan alamat aplikan harus diisi!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Ya';
            },
            {
              'id': 2;
              'label': 'Tidak';
            },
            {
              'id': 99;
              'label': 'Tidak Tahu';
            }
          ];
          'value': '';
        },
        {
          'id': 42;
          'type': 'select';
          'form': 'StatusKepemilikan';
          'label': 'Status Kepemilikan';
          'error': 'Status Kepemilikan harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'KeteranganStatusKepemilikan';
            'label': 'Keterangan Status Kepemilikan';
            'error': 'Keterangan status kepemilikan harus diisi!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Sesuai';
            },
            {
              'id': 2;
              'label': 'Tidak';
            },
            {
              'id': 99;
              'label': 'Tidak Tahu';
            }
          ];
          'value': '';
        },
        {
          'id': 43;
          'type': 'select';
          'form': 'LamaTinggal';
          'label': 'Lama Tinggal';
          'error': 'Lama Tinggal harus di pilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'KeteranganLamaTinggal';
            'label': 'keterangan Lama Tinggal';
            'error': 'Keterangan lama tinggal harus diisi!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Sesuai';
            },
            {
              'id': 2;
              'label': 'Tidak';
            },
            {
              'id': 99;
              'label': 'Tidak Tahu';
            }
          ];
          'value': '';
        }
      ];
    },
    {
      'name': 'Hasil Pengamatan Saat Survey';
      'tag': 'InputSurveyStep5';
      'details': [
        {
          'id': 44;
          'type': 'select';
          'form': 'KondisiBangunanRumah';
          'label': 'Kondisi Bangunan Rumah';
          'error': 'Kondisi bangunan rumah harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Baik/Terawat';
            },
            {
              'id': 2;
              'label': 'Sedang/Kurang Terawat';
            },
            {
              'id': 3;
              'label': 'Rusak/Tidak Terawat';
            }
          ];
          'value': '';
        },
        {
          'id': 45;
          'type': 'inputWithLabel';
          'form': null;
          'label': 'Luas Tanah';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'LuasTanahPanjang';
            'label': 'M';
            'error': 'Panjang tanah tidak boleh kosong!';
            'placeHolder': 'Panjang';
            'value': null;
          };
          'ext2': {
            'type': 'text';
            'form': 'LuasTanahLebar';
            'label': 'M';
            'error': 'Lebar tanah tidak boleh kosong!';
            'placeHolder': 'Lebar';
            'value': null;
          };
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 46;
          'type': 'inputWithLabel';
          'form': null;
          'label': 'Luas Bangunan';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'LuasBangunanPanjang';
            'label': 'M';
            'error': 'Panjang bangunan tidak boleh kosong!';
            'placeHolder': 'Panjang';
            'value': null;
          };
          'ext2': {
            'type': 'text';
            'form': 'LuasBangunanLebar';
            'label': 'M';
            'error': 'Lebar bangunan tidak boleh kosong!';
            'placeHolder': 'Lebar';
            'value': null;
          };
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 47;
          'type': 'select';
          'form': 'JumlahTingkat';
          'label': 'Jumlah Tingkat';
          'error': 'Jumlah tingkat harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': '1';
            },
            {
              'id': 2;
              'label': '2';
            },
            {
              'id': 3;
              'label': '3';
            },
            {
              'id': 4;
              'label': '>3';
            }
          ];
          'value': '';
        },
        {
          'id': 48;
          'type': 'inputWithLabel';
          'form': null;
          'label': 'Lebar Jalan';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'Lebarjalan';
            'label': 'M';
            'error': 'Lebar jalan tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 49;
          'type': 'inputWithLabel';
          'form': null;
          'label': 'Jumlah Lantai Rumah';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'JumlahLantaiRumah';
            'label': 'Tingkat';
            'error': 'Jumlah lantai rumah tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 50;
          'type': 'select';
          'form': 'KondisiDindingRumah';
          'label': 'Kondisi Dinding Rumah';
          'error': 'Kondisi dinding rumah harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'KondisiDindingRumahLainnya';
            'label': 'Kondisi Dinding Rumah Lainnya';
            'error': 'Kondisi dinding rumah lainnya tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Tembok Diplester';
            },
            {
              'id': 2;
              'label': 'Tembok Tidak Diplester';
            },
            {
              'id': 3;
              'label': 'Sebagian/Seluruhnya Tidak Disemen';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 51;
          'type': 'select';
          'form': 'KondisiAtapRumah';
          'label': 'Kondisi Atap Rumah';
          'error': 'Kondisi atap rumah harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'KondisiAtapRumahLainnya';
            'label': 'Kondisi Atap Rumah Lainnya';
            'error': 'Kondisi atap rumah lainnya tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Genteng';
            },
            {
              'id': 2;
              'label': 'Asbes/Seng';
            },
            {
              'id': 3;
              'label': 'Kayu/Sirap';
            },
            {
              'id': 99;
              'label': 'Lainnya';
            }
          ];
          'value': '';
        },
        {
          'id': 52;
          'type': 'select';
          'form': 'KendaraanYangDimiliki';
          'label': 'Kendaraan Yang Dimiliki';
          'error': 'Kendaraan Yang Dimiliki harus dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': {
            'type': 'text';
            'form': 'JumlahKendaraan';
            'label': 'Jumlah Kendaraan';
            'error': 'Jumlah kendaraan tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext2': {
            'type': null;
            'form': 'JenisKendaraan';
            'label': 'Jenis Kendaraan';
            'error': 'Jenis kendaraan tidak boleh kosong!';
            'placeHolder': null;
            'value': null;
          };
          'ext3': null;
          'data': [
            {
              'id': 200;
              'label': 'Ya';
            },
            {
              'id': 1;
              'label': 'Tidak';
            }
          ];
          'value': '';
        },
        {
          'id': 53;
          'type': 'select';
          'form': 'PencarianAlamat';
          'label': 'Pencarian Alamat';
          'error': 'Pencarian alamat perlu dipilih!';
          'placeholder': 'Pilih...';
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': [
            {
              'id': 1;
              'label': 'Mudah';
            },
            {
              'id': 2;
              'label': 'Sulit';
            },
            {
              'id': 3;
              'label': 'Tidak Terlacak';
            }
          ];
          'value': '';
        }
      ];
    },
    {
      'name': 'Map Rumah';
      'tag': 'InputSurveyStep6';
      'details': [
        {
          'id': 54;
          'type': 'map';
          'form': null;
          'label': 'Map';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': {
            'type': 'decimal';
            'form': 'longitude';
            'label': 'longitude';
            'error': 'Longitude harus diisi! Klik pada map untuk mengisi.';
            'placeHolder': null;
            'value': null;
          };
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        }
      ];
    },
    {
      'name': 'Upload Gambar Rumah';
      'tag': 'InputSurveyStep7';
      'details': [
        {
          'id': 55;
          'type': 'upload';
          'form': null;
          'label': 'Upload';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        }
      ];
    },
    {
      'name': 'Surveyor';
      'tag': 'InputSurveyStep8';
      'details': [
        {
          'id': 56;
          'type': 'readOnlyText';
          'form': 'Code';
          'label': 'Kode';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 57;
          'type': 'readOnlyText';
          'form': 'Nama';
          'label': 'Nama';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 58;
          'type': 'readOnlyText';
          'form': 'Tanggal';
          'label': 'Tanggal';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        },
        {
          'id': 59;
          'type': 'readOnlyText';
          'form': 'Jam';
          'label': 'Jam';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        }
      ];
    },
    {
      'name': 'Catatan';
      'tag': 'InputSurveyStep9';
      'details': [
        {
          'id': 60;
          'type': 'textarea';
          'form': 'Catatan';
          'label': 'Catatan';
          'error': null;
          'placeholder': null;
          'labelDivider': null;
          'ext1': null;
          'ext2': null;
          'ext3': null;
          'data': null;
          'value': '';
        }
      ];
    }
  ];
}
