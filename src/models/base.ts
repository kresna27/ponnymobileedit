export class BaseResponse {
  isSuccess: boolean;
  value: any;
  errorMessage: string;
}
