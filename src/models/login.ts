export class LoginRequest {
  Email: string;
  Password: string;
}

export class LoginResponse {
  token: string;
  expiration: string;
}
